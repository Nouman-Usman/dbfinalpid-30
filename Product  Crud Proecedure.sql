
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE Product_Crud
	@Action VARCHAR(15),
	@ProductID INT = NULL,
	@ProductyName VARCHAR(100) = NULL,
	@ShortDescription VARCHAR(100) = NULL,
	@LongDescription VARCHAR(MAX) = NULL,
	@AdditionalDescription VARCHAR(100) = NULL,
	@Price INT = NULL,
	@Quantity INT = NULL,
	@Size VARCHAR(5) = NULL,
	@Color VARCHAR(10) = NULL,
	@CompanyName VARCHAR(20) = NULL,
	@CategoryId INT = NULL,
	@SubCategoryId INT = NULL,
	@Sold INT = NULL,
	@IsCustomized BIT = False,
	@IsActive BIT = False
AS
BEGIN
	SET NOCOUNT ON;
	if (@Action = 'GETALL')
	BEGIN
		Select * From Category
	END
	if(@Action = 'GETBYID')
	BEGIN
	Select * from Product p
	Where p.ProductId = @ProductID;
	END
	if (@Action = 'INSERT')
	BEGIN
	INSERT  INTO Product(ProductName,ShortDescription,LongDescription,AdditionalDescription,Price,Quantity,Size,Color,CompanyName,CategoryId,SubCategoryId,Sold,IsCustiomized,IsActive,CreatedDate)
	VALUES(@ProductyName, @ShortDescription,@LongDescription,@AdditionalDescription,@Price,@Quantity,@Size,@Color,@CompanyName,@CategoryId,@SubCategoryId,@Sold,@IsCustomized,@IsActive,GETDATE())
	END
	if(@Action = 'UPDATE')
	BEGIN
	UPDATE Product
    SET 
        ProductName = ISNULL(@ProductyName, ProductName),
        ShortDescription = @ShortDescription,
        LongDescription = @LongDescription,
        AdditionalDescription = @AdditionalDescription,
        Price = @Price,
        Quantity = @Quantity,
        Size = @Size,
        Color = @Color,
        CompanyName = @CompanyName,
        CategoryId = @CategoryId,
        SubCategoryId = @SubCategoryId,
        Sold = @Sold,
        IsCustiomized = @IsCustomized,
        IsActive = @IsActive
    WHERE ProductId = @ProductID;
	END
	if (@Action = 'DELETE')
	BEGIN
		DELETE From Product 
		WHERE ProductId = @ProductID
	END
	if (@Action = 'ACTIVEPRODUCT')
	BEGIN
		SELECT * From Product c
		WHERE c.IsActive = 1 ORDER BY c.ProductName
	END
END
GO
