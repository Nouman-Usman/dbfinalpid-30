﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace JSM.Admin
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            totalOrders();
            totalEarning();
            totalDelivered();
            pendingsOrders();
            //if (!IsPostBack)
            //{
            //    // Fetch data from database or any other source
            //    DataTable earningsData = GetEarningsByLocation();

            //    // Bind data to HTML elements
            //    if (earningsData != null && earningsData.Rows.Count > 0)
            //    {
            //        for (int i = 0; i < earningsData.Rows.Count; i++)
            //        {
            //            string location = earningsData.Rows[i]["Location"].ToString();
            //            int earningPercentage = Convert.ToInt32(earningsData.Rows[i]["EarningPercentage"]);

            //            // Update location and percentage directly
            //            // Assuming the container div has an ID "visitbylocate"
            //            LiteralControl locationControl = new LiteralControl("<span class='text-muted font-14'>" + location + "</span>");
            //            LiteralControl percentageControl = new LiteralControl("<span class='mb-0 font-14 text-dark font-weight-medium'>" + earningPercentage + "%</span>");

            //            // Find the container div by ID and add location and percentage controls
            //            HtmlGenericControl containerDiv = (HtmlGenericControl)FindControl("visitbylocate");
            //            containerDiv.Controls.Add(locationControl);
            //            containerDiv.Controls.Add(percentageControl);

            //            // Update progress bar width dynamically
            //            // Assuming the progress bar div has a class "progress-bar" and is a direct child of the container div
            //            System.Web.UI.HtmlControls.HtmlGenericControl progressBar = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            //            progressBar.Attributes["class"] = "progress-bar";
            //            progressBar.Attributes["style"] = "width: " + earningPercentage + "%";

            //            // Add the progress bar to the container div
            //            containerDiv.Controls.Add(progressBar);
            //        }
            //    }
            //}

        }
        public void totalOrders()
        {
            SqlConnection con = new SqlConnection(Utils.getConnection());
            SqlCommand cmd = new SqlCommand("SELECT Count(*) FROM Orders");
            cmd.Connection = con;
            con.Open();
            int count = (int)cmd.ExecuteScalar();
            con.Close();
            lblTotalOrders.Text = count.ToString();
        }
        public void totalEarning()
        {
            SqlConnection con = new SqlConnection(Utils.getConnection());
            SqlCommand cmd = new SqlCommand("SELECT SUM(Orders.Quantity * Product.Price) AS TotalAmount\r\nFROM Orders\r\nJOIN Product ON Orders.ProductId = Product.ProductId\r\n\r\nWhere Orders.Status = 'Delivered'");
            cmd.Connection = con;
            con.Open();
            string count = (cmd.ExecuteScalar()).ToString();
            con.Close();
            if(count != "")
            {
                //directSale.Text = count;
                lblTotalEarning.Text = count;
            }
            else
            {
                //directSale.Text = "0";
                lblTotalEarning.Text = "0";
            }
            //lblTotalEarning.Text = count.ToString();
        }
        void totalDelivered ()
        {
            SqlConnection con = new SqlConnection(Utils.getConnection());
            SqlCommand cmd = new SqlCommand("SELECT Count(*) FROM Orders WHERE Status = 'Delivered'");
            cmd.Connection = con;
            con.Open();
            int count = (int)cmd.ExecuteScalar();
            con.Close();
            lblTotalDelivered.Text = count.ToString();
        }
        void pendingsOrders()
        {
            SqlConnection con = new SqlConnection(Utils.getConnection());
            SqlCommand cmd = new SqlCommand("SELECT Count(*) FROM Orders WHERE Status = 'pending'");
            cmd.Connection = con;
            con.Open();
            int count = (int)cmd.ExecuteScalar();
            con.Close();
            lblPending.Text = count.ToString();
        }
        private DataTable GetEarningsByLocation()
        {
            // Assuming you have a method to fetch data from the database and return it as a DataTable
            // Replace this with your actual data retrieval logic
            DataTable earningsData = new DataTable();

            // Sample data for demonstration purposes
            earningsData.Columns.Add("Location", typeof(string));
            earningsData.Columns.Add("EarningPercentage", typeof(int));
            earningsData.Rows.Add("Pakistan", 22);
            earningsData.Rows.Add("UK", 12);
            earningsData.Rows.Add("USA", 9);
            earningsData.Rows.Add("China", 8);

            return earningsData;
        }
    }
}