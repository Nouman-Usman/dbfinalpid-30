﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.Admin
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter sda;
        DataTable da;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmsg.Visible = false;
            GetCategory();
        }
        protected void btnAddOrUpdate_Click1(object sender, EventArgs e)
        {
            string actionName = string.Empty, imagePath = string.Empty, fileExtension = string.Empty;
            bool isValidToExecute = false;
            int CategoryID = Convert.ToInt32(hfCategoryId.Value);
            con = new SqlConnection(Utils.getConnection());
            cmd = new SqlCommand("Category_Crud", con);
            cmd.Parameters.AddWithValue("@Action", CategoryID == 0 ? "INSERT" : "UPDATE");
            cmd.Parameters.AddWithValue("@CategoryId", CategoryID);
            cmd.Parameters.AddWithValue("@CategoryName", txtCategory.Text.Trim());
            cmd.Parameters.AddWithValue("@IsActive", cbIsActive.Checked);
            if (fuCategoryImage.HasFile)
            {
                fileExtension = System.IO.Path.GetExtension(fuCategoryImage.FileName);
                if (Utils.isValidExtension(fuCategoryImage.FileName))
                {
                    string newImageName = Utils.getUniqueId();
                    fileExtension = Path.GetExtension(fuCategoryImage.FileName);
                    imagePath = "../Images/Category/" + newImageName.ToString() + fileExtension;
                    fuCategoryImage.PostedFile.SaveAs(Server.MapPath("../Images/Category/") + newImageName.ToString() + fileExtension);
                    cmd.Parameters.AddWithValue("@CategoryImageUrl", imagePath);
                    isValidToExecute = true;
                }
                else
                {
                    isValidToExecute = false;
                    lblmsg.Visible = true;
                    lblmsg.Text = "Please select only .jpg, .png, .jpeg";
                    lblmsg.CssClass = "alert alert-danger";
                }
            }
            else
            {
                isValidToExecute = true;
            }
            if (isValidToExecute)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    actionName = CategoryID == 0 ? "Inserted" : "Updated";
                    lblmsg.Visible = true;
                    lblmsg.Text = "Category " + actionName + " Successfully";
                    lblmsg.CssClass = "alert alert-success";
                    Clear();
                    isValidToExecute = false;
                    GetCategory();
                }
                catch (Exception ex)
                {
                    lblmsg.Visible = true;
                    lblmsg.Text = "Error: " + ex.Message;
                    lblmsg.CssClass = "alert alert-danger";
                }
                finally
                {
                    con.Close();
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
        }
        void Clear()
        {
            txtCategory.Text = string.Empty;
            cbIsActive.Checked = false;
            hfCategoryId.Value = "0";
            btnAddOrUpdate.Text = "Add";
            fuCategoryImage.Attributes.Clear();
        }

        void GetCategory()
        {
            // I want to clear previo data from repeater
            rCategory.DataSource = null;
            rCategory.DataBind();
            con = new SqlConnection(Utils.getConnection());
            cmd = new SqlCommand("Category_Crud", con);
            cmd.Parameters.AddWithValue("@Action", "GETALL");
            cmd.CommandType = CommandType.StoredProcedure;
            sda = new SqlDataAdapter(cmd);
            da = new DataTable();
            sda.Fill(da);
            rCategory.DataSource = da;
            rCategory.DataBind();
        }

        protected void rCategory_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            
            lblmsg.Visible = false;
            if (e.CommandName == "edit")
            {
                con = new SqlConnection(Utils.getConnection());
                cmd = new SqlCommand("Category_Crud", con);
                cmd.Parameters.AddWithValue("@Action", "GETBYID");
                cmd.Parameters.AddWithValue("@CategoryId", e.CommandArgument);
                cmd.CommandType = CommandType.StoredProcedure;
                sda = new SqlDataAdapter(cmd);
                da = new DataTable();
                sda.Fill(da);
                btnAddOrUpdate.Text = "Update";
                txtCategory.Text = da.Rows[0]["CategoryName"].ToString();
                cbIsActive.Checked = Convert.ToBoolean(da.Rows[0]["IsActive"]);
                imagePreview.ImageUrl = string.IsNullOrEmpty(da.Rows[0]
                    ["CategoryImageUrl"].ToString()) ? "../Images/No_image.png" : "../" + da.Rows[0]
                    ["CategoryImageUrl"].ToString();
                imagePreview.Height = 200;
                imagePreview.Width = 200;
                hfCategoryId.Value = da.Rows[0]["CategoryID"].ToString();
            }
            else if (e.CommandName == "delete")
            {
                con = new SqlConnection(Utils.getConnection());
                cmd = new SqlCommand("Category_Crud", con);
                cmd.Parameters.AddWithValue("@Action", "DELETE");
                cmd.Parameters.AddWithValue("@CategoryId", e.CommandArgument);
                cmd.CommandType = CommandType.StoredProcedure;
                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    lblmsg.Visible = true;
                    lblmsg.Text = "Category Deleted Successfully";
                    lblmsg.CssClass = "alert alert-danger";
                    GetCategory();
                }
                catch (Exception ex)
                {
                    lblmsg.Visible = true;
                    lblmsg.Text = "Error: " + ex.Message;
                    lblmsg.CssClass = "alert alert-danger";
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}