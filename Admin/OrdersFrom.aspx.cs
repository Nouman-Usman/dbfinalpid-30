﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.Admin
{
    public partial class OrdersFrom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }
        void LoadData()
        {
            int UserId = Convert.ToInt32(Request.QueryString["UserId"]);
            pdTable.DataSource = null;
            pdTable.DataBind();
            SqlConnection con = new SqlConnection(Utils.getConnection());
            SqlCommand cmd = new SqlCommand("shipping_Crud", con);
            cmd.Parameters.AddWithValue("@Action", "LOAD");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable da = new DataTable();
            sda.Fill(da);
            pdTable.DataSource = da;
            pdTable.DataBind();
        }

        protected void pdTable_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Dispatched")
            {
                // I want to update the status of the order to dispatched
                string orderId = (e.CommandArgument).ToString();
                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand("shipping_Crud", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Action", "UPDATE");
                        cmd.Parameters.AddWithValue("@OrderNo", orderId);
                        cmd.Parameters.AddWithValue("@Status", "Dispatched");
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        LoadData();
                    }
                }
            }
            else if (e.CommandName == "Delivered")
            {
                string orderId = (e.CommandArgument).ToString();
                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand("shipping_Crud", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Action", "UPDATE");
                        cmd.Parameters.AddWithValue("@OrderNo", orderId);
                        cmd.Parameters.AddWithValue("@Status", "Delivered");
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        LoadData();
                    }
                }
            }
        }
    }
}