﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.Admin
{
    public partial class AdminLogin : System.Web.UI.Page
    {
        string previousPageUrl = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (previousPageUrl == null && Request.UrlReferrer != null)
            {
                // I want to redirect to dashboard page
                previousPageUrl = "Dashboard.aspx";
            }
            else if (Request.QueryString["PreviousUrl"] != null)
            {
                // dashboard page
                previousPageUrl = Request.QueryString["PreviousUrl"].ToString(); 
            }

        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            string email = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();
            string roleName = string.Empty;
            string userId = string.Empty;
            using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
            {
                using (SqlCommand command = new SqlCommand("SELECT r.RoleName, Users.UserId from Users Inner JOIN Roles as r on Users.RoleId = r.RoleId WHERE Email = @Email AND Password = @Password", connection))
                {
                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@Password", password);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        roleName = reader["RoleName"].ToString();
                        userId = reader["UserId"].ToString();
                    }

                    reader.Close();
                }
            }
            if (!string.IsNullOrEmpty(roleName))
            {
                if (roleName == "Admin")
                {
                    Response.Redirect($"{previousPageUrl}?&AdminId={userId}");
                }
                else
                {
                    lblError.Text = "Invalid username or password";
                    lblError.CssClass = "text-danger";
                }
            }
            else
            {
                lblError.Text = "No username or password Registered";
                lblError.CssClass = "text-danger";
            }
        }
    }
}