﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="OrdersFrom.aspx.cs" Inherits="JSM.Admin.OrdersFrom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Order Details</h4>
        <hr />
        <div class="table-responsive">
            <asp:Repeater ID="pdTable" runat="server" OnItemCommand="pdTable_ItemCommand">
                <HeaderTemplate>
                    <table class="table data-table-export table-hover nowrap">
                        <thead>
                            <tr>
                                <th class="table-plus">Username</th> 
                                <th class="table-plus">Product Name</th>
                                <th>OrderNo</th>
                                <th>Order Date</th>
                                <th>Delivery Adress</th>
                                <th>Quantity</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="table-plus"><%# Eval("Username") %></td>
                        <td class="table-plus"><%# Eval("ProductName") %></td>
                        <td class="table-plus"><%# Eval("OrderNo") %></td>
                        <td><%# Eval("OrderDate") %></td>
                        <td><%# Eval("AddressLine1")%></td>
                        <td><%# Eval("Quantity") %></td>
                        <td>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' CssClass="badge badge-primary"></asp:Label>
                        </td>
                        <td>
                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CssClass="badge badge-primary"
                                CommandArgument='<%# Eval("OrderNo") %>' CommandName="Dispatched" CausesValidation="false">
                                <i class="fas fa-truck"></i>
                                Dispatched
                            </asp:LinkButton>
                            <asp:LinkButton ID="lblDelivered" runat="server" Text="Delete" CssClass="badge badge-danger"
                                CommandArgument='<%# Eval("OrderNo") %>' CommandName="Delivered" CausesValidation="false">
                                <i class="fas fa-check-circle"></i>
                                Delivered
                            </asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                        </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
</asp:Content>
