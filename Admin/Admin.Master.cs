﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.Admin
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string currentUrl = $"{Request.Url.AbsoluteUri.ToString()}?";
            if (Request.Url.AbsoluteUri.Contains("AdminId"))
            {
                int userId = Convert.ToInt32(Request.QueryString["AdminId"]);
                if (userId == 0)
                {
                    Response.Redirect($"Login.aspx?PreviousUrl={Server.UrlEncode(currentUrl)}");
                }
                else
                {
                    // retrieve the username from the database corresponding to the userId
                    // and display it in the welcome message
                    // lblWelcome.Text = "Welcome, " + username;
                    using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                    {
                        using (SqlCommand cmd = new SqlCommand("SELECT UserName FROM Users WHERE UserId = @UserId", con))
                        {
                            cmd.Parameters.AddWithValue("@UserId", userId);
                            con.Open();
                            lblWelcome.Text = "Welcome, " + cmd.ExecuteScalar().ToString();
                            con.Close();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect($"Login.aspx?PreviousUrl={Server.UrlEncode(currentUrl)}");
            }
        }
        protected string GetUserName()
        {
            int userId = Convert.ToInt32(Request.QueryString["AdminId"]);
            using (SqlConnection con = new SqlConnection(Utils.getConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT UserName FROM Users WHERE UserId = @UserId", con))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    con.Open();
                    return cmd.ExecuteScalar().ToString();
                }
            }
        }
    }
}