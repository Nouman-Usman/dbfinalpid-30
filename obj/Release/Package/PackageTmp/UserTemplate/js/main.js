(function ($) {
    "use strict";
    
    // Dropdown on mouse hover
    $(document).ready(function () {
        function toggleNavbarMethod() {
            if ($(window).width() > 992) {
                $('.navbar .dropdown').on('mouseover', function () {
                    $('.dropdown-toggle', this).trigger('click');
                }).on('mouseout', function () {
                    $('.dropdown-toggle', this).trigger('click').blur();
                });
            } else {
                $('.navbar .dropdown').off('mouseover').off('mouseout');
            }
        }
        toggleNavbarMethod();
        $(window).resize(toggleNavbarMethod);
    });
    
    
    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({scrollTop: 0}, 1500, 'easeInOutExpo');
        return false;
    });


    // Vendor carousel
    $('.vendor-carousel').owlCarousel({
        loop: true,
        margin: 29,
        nav: false,
        autoplay: true,
        smartSpeed: 1000,
        responsive: {
            0:{
                items:2
            },
            576:{
                items:3
            },
            768:{
                items:4
            },
            992:{
                items:5
            },
            1200:{
                items:6
            }
        }
    });


    // Related carousel
    $('.related-carousel').owlCarousel({
        loop: true,
        margin: 29,
        nav: false,
        autoplay: true,
        smartSpeed: 1000,
        responsive: {
            0:{
                items:1
            },
            576:{
                items:2
            },
            768:{
                items:3
            },
            992:{
                items:4
            }
        }
    });


    // Product Quantity
    $('.quantity button').on('click', function () {
        var button = $(this);
        var oldValue = button.parent().parent().find('input').val();
        if (button.hasClass('btn-plus')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        button.parent().parent().find('input').val(newVal);
    });
    
})(jQuery);
// Event listener for checkbox changes
document.querySelectorAll('input[type="checkbox"]').forEach(function (checkbox) {
    checkbox.addEventListener('change', function () {
        // Check which checkboxes are checked and filter products accordingly
        if (document.getElementById('price-all').checked) {
            // If "All Price" checkbox is checked, show all products
            document.querySelectorAll('.product-item').forEach(function (product) {
                product.style.display = 'block';
            });
        } else {
            // If specific price ranges are checked, filter products based on the selected range
            var minPrice, maxPrice;
            if (document.getElementById('price-1').checked) {
                minPrice = 0;
                maxPrice = 100;
            }
            // Repeat similar logic for other price ranges

            // Call server-side method to load products based on selected price range
            loadProductsByPrice(minPrice, maxPrice);
        }
    });
});

// Function to load products based on price range from server-side
function loadProductsByPrice(minPrice, maxPrice) {
    // Make an AJAX request to call server-side method and load products
    // Example using jQuery AJAX
    $.ajax({
        type: 'POST',
        url: 'Shop.aspx/LoadProductsByPrice', // Update with your ASPX page and method name
        data: JSON.stringify({ minPrice: minPrice, maxPrice: maxPrice }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            // Handle success response and update UI with filtered products
        },
        error: function (xhr, status, error) {
            // Handle error
        }
    });
}


