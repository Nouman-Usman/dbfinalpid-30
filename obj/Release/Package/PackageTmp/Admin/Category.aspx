﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="JSM.Admin.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        window.onload = function () {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<% = lblmsg.ClientID %>").style.display = "none";
            }, seconds * 1000)
        }
    </script>
    <script>
        function ImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imagePreview.ClientID %>').prop('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="md-4">
        <asp:Label ID="lblmsg" runat="server"></asp:Label>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Category</h4>
                        <hr />
                        <div class="form-body">
                            <label>Category Name</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control" placeholder="Enter Category Name"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCategory" ErrorMessage="Category Name is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>

                            <label>Category Image</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:FileUpload ID="fuCategoryImage" runat="server" CssClass="form-control" onchange="ImagePreview(this);" />
                                        <asp:HiddenField ID="hfCategoryId" runat="server" Value="0" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="fuCategoryImage" ErrorMessage="Category Name is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:CheckBox ID="cbIsActive" runat="server" Text="&nbsp; Active" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-action pb-5">
                                <div class="text-left">
                                    <asp:Button ID="btnAddOrUpdate" runat="server" CssClass="btn btn-info" Text="Add" OnClick="btnAddOrUpdate_Click1" />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-danger" Text="Reset" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                            <div>
                                <asp:Image ID="imagePreview" runat="server" CssClass="img-thumbnail" AlternateText="Empty" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Category List</h4>
                        <hr />
                        <div class="table-responsive">
                            <asp:Repeater ID="rCategory" runat="server" OnItemCommand="rCategory_ItemCommand">
                                <HeaderTemplate>
                                    <table class="table data-table-export table-hover nowrap">
                                        <thead>
                                            <tr>
                                                <th class="table-plus">Name</th>
                                                <th>Image</th>
                                                <th>Is Active</th>
                                                <th>Created Date</th>
                                                <th class="datatable-nosort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="table-plus"><%# Eval("CategoryName") %></td>
                                        <td>
                                            <img width="40" src="<%# JSM.Utils.getImageUrl(Eval("CategoryImageUrl")) %>" alt="image" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblIsActive" runat="server" Text='<%# (bool) Eval("IsActive") == true ? "Active": "InActive" %>' CssClass='<%# (bool) Eval("IsActive") == true ? "badge badge-success": "badge badge-danger" %>'></asp:Label>
                                        </td>
                                        <td>
                                            <%# Eval("CreatedDate") %>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CssClass="badge badge-primary"
                                                CommandArgument='<%# Eval("CategoryId") %>' CommandName="edit" CausesValidation="false">
                                               <i class ="fas fa-edit">
                                               </i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lblDelete" runat="server" Text="Delete" CssClass="badge badge-danger"
                                                CommandArgument='<%# Eval("CategoryId") %>' CommandName="delete" CausesValidation="false">
                                            <i class ="fas fa-trash-alt"></i>
                                            </asp:LinkButton>

                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        
            </div>
</asp:Content>
