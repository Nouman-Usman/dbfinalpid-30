USE [master]
GO
/****** Object:  Database [JSM]    Script Date: 5/10/2024 9:31:32 AM ******/
CREATE DATABASE [JSM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JSM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\JSM.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'JSM_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\JSM_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [JSM] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JSM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [JSM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [JSM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [JSM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [JSM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [JSM] SET ARITHABORT OFF 
GO
ALTER DATABASE [JSM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [JSM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [JSM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [JSM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [JSM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [JSM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [JSM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [JSM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [JSM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [JSM] SET  ENABLE_BROKER 
GO
ALTER DATABASE [JSM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [JSM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [JSM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [JSM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [JSM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [JSM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [JSM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [JSM] SET RECOVERY FULL 
GO
ALTER DATABASE [JSM] SET  MULTI_USER 
GO
ALTER DATABASE [JSM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [JSM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [JSM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [JSM] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [JSM] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [JSM] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'JSM', N'ON'
GO
ALTER DATABASE [JSM] SET QUERY_STORE = ON
GO
ALTER DATABASE [JSM] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [JSM]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[CartId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Quantity] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cartdetails]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cartdetails](
	[cartdetail_id] [int] IDENTITY(1,1) NOT NULL,
	[color] [varchar](50) NULL,
	[size] [varchar](50) NULL,
	[UserId] [int] NULL,
	[cartID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[cartdetail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](100) NOT NULL,
	[CategoryImageUrl] [varchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Subject] [varchar](200) NULL,
	[Message] [varchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[OrderNo] [varchar](max) NULL,
	[ProductId] [int] NOT NULL,
	[Quantity] [int] NULL,
	[UserId] [int] NOT NULL,
	[Status] [varchar](50) NULL,
	[OrderDate] [datetime] NOT NULL,
	[IsCancel] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payment]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[PaymentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[CardNo] [varchar](50) NULL,
	[ExpiryDate] [varchar](50) NULL,
	[CvvNo] [int] NULL,
	[Adress] [varchar](50) NULL,
	[PaymentMode] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](100) NOT NULL,
	[ShortDescription] [varchar](200) NULL,
	[LongDescription] [varchar](max) NULL,
	[AdditionalDescription] [varchar](max) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[Size] [varchar](30) NULL,
	[Color] [varchar](50) NULL,
	[CompanyName] [varchar](100) NULL,
	[CategoryId] [int] NOT NULL,
	[Sold] [int] NULL,
	[IsCustiomized] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[productAddImages]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productAddImages](
	[ProductId] [int] NULL,
	[image1] [varchar](max) NULL,
	[image2] [varchar](max) NULL,
	[image3] [varchar](max) NULL,
	[image4] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductImages]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductImages](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[ImageUrl] [varchar](max) NOT NULL,
	[ProductId] [int] NOT NULL,
	[DefaultImage] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductReview]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductReview](
	[ReviewId] [int] IDENTITY(1,1) NOT NULL,
	[Rating] [int] NOT NULL,
	[Comment] [varchar](max) NULL,
	[ProductId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ReviewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] NOT NULL,
	[RoleName] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShippingDetails]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShippingDetails](
	[ShippingDetailID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](100) NULL,
	[MobileNo] [nvarchar](20) NOT NULL,
	[AddressLine1] [nvarchar](100) NOT NULL,
	[AddressLine2] [nvarchar](100) NULL,
	[Country] [nvarchar](50) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[State] [nvarchar](50) NOT NULL,
	[ZIPCode] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ShippingDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategory](
	[CategoryID] [int] NOT NULL,
	[SubCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[SubCategoryName] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SubCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserNetworkInfo]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserNetworkInfo](
	[NetworkInfoId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[MacAddress] [varchar](17) NULL,
	[IpAddress] [varchar](45) NULL,
PRIMARY KEY CLUSTERED 
(
	[NetworkInfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Username] [varchar](50) NULL,
	[Mobile] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[Adress] [varchar](max) NULL,
	[PostCode] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[ImageUrl] [varchar](max) NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Wishlist]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wishlist](
	[WishlistId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[WishlistId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryID], [CategoryName], [CategoryImageUrl], [IsActive], [CreatedDate]) VALUES (1, N'pents', N'../Images/Category/de963c5a-e1dd-4260-bfa4-c298a5ccf70d.jpeg', 1, CAST(N'2024-05-03T19:35:06.043' AS DateTime))
INSERT [dbo].[Category] ([CategoryID], [CategoryName], [CategoryImageUrl], [IsActive], [CreatedDate]) VALUES (2, N'Oversized Shirts', N'../Images/Category/5b5e2fe6-4654-4535-90af-5c53da3f3b70.jpg', 1, CAST(N'2024-05-03T19:35:32.377' AS DateTime))
INSERT [dbo].[Category] ([CategoryID], [CategoryName], [CategoryImageUrl], [IsActive], [CreatedDate]) VALUES (1002, N'Jewlery', N'../Images/Category/beca5e64-f23b-4459-bd5e-7ad6b0f25ba9.png', 1, CAST(N'2024-05-06T18:59:52.280' AS DateTime))
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderDetailsId], [OrderNo], [ProductId], [Quantity], [UserId], [Status], [OrderDate], [IsCancel]) VALUES (1006, N'ORD35-54810', 4, 3, 35, N'Delivered', CAST(N'2024-05-09T03:57:23.640' AS DateTime), 0)
INSERT [dbo].[Orders] ([OrderDetailsId], [OrderNo], [ProductId], [Quantity], [UserId], [Status], [OrderDate], [IsCancel]) VALUES (1007, N'ORD35-54810', 1, 5, 35, N'Delivered', CAST(N'2024-05-09T04:02:19.040' AS DateTime), 0)
INSERT [dbo].[Orders] ([OrderDetailsId], [OrderNo], [ProductId], [Quantity], [UserId], [Status], [OrderDate], [IsCancel]) VALUES (1008, N'ORD35-548104601', 4, 3, 35, N'Delivered', CAST(N'2024-05-09T20:28:47.370' AS DateTime), 0)
INSERT [dbo].[Orders] ([OrderDetailsId], [OrderNo], [ProductId], [Quantity], [UserId], [Status], [OrderDate], [IsCancel]) VALUES (1009, N'ORD35-548104601', 4, 4, 35, N'Delivered', CAST(N'2024-05-09T20:28:47.370' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ProductId], [ProductName], [ShortDescription], [LongDescription], [AdditionalDescription], [Price], [Quantity], [Size], [Color], [CompanyName], [CategoryId], [Sold], [IsCustiomized], [IsActive], [CreatedDate]) VALUES (1, N'Off sholder T-Shirt', N'', N'', N'', CAST(1200.00 AS Decimal(18, 2)), 12, N'S,M,L', N'Black,Whit', N'', 1, 0, 0, 1, CAST(N'2024-05-03T19:38:38.930' AS DateTime))
INSERT [dbo].[Product] ([ProductId], [ProductName], [ShortDescription], [LongDescription], [AdditionalDescription], [Price], [Quantity], [Size], [Color], [CompanyName], [CategoryId], [Sold], [IsCustiomized], [IsActive], [CreatedDate]) VALUES (2, N'Sleeveless', N'Very Unique Seleevless T-Shirt', N'We urge you to mark your presence at the venue 45 minutes prior to event timing to carry out necessary verification.
All attendees must carry e-ticket or its printout for verification at the venue.
Please carry a valid government identity card to attain a conference attendee badge at the venue.
The attendees must wear their conference badge throughout the event.
Seat numbers will be allotted to each confirmed and verified attendee at the entrance.
Entry into the venue will only start 30 minutes before the event timing.
The venue doors will remain shut during hackathon and will not open for entry.
Attendees reaching late at the venue will not be allowed to attend the event.
Re-entry into the venue is strictly prohibited.', N'The organisers reserve the rights of frisk and restrict entry.
Please cooperate with the private se', CAST(12.00 AS Decimal(18, 2)), 11, N'S,M,L', N'Black,Blue', N'Denim', 2, 0, 1, 1, CAST(N'2024-05-03T22:09:19.047' AS DateTime))
INSERT [dbo].[Product] ([ProductId], [ProductName], [ShortDescription], [LongDescription], [AdditionalDescription], [Price], [Quantity], [Size], [Color], [CompanyName], [CategoryId], [Sold], [IsCustiomized], [IsActive], [CreatedDate]) VALUES (3, N'Dragon Pent', N'Dragin Pent with handwritten notes', N'gfdgfdgfd', N'fksdjfnkjsdgfkjsdgknfdgkg', CAST(1100.00 AS Decimal(18, 2)), 20, N'S,M,L', N'Black,Blue', N'Denim', 2, 0, 0, 1, CAST(N'2024-05-04T10:17:03.100' AS DateTime))
INSERT [dbo].[Product] ([ProductId], [ProductName], [ShortDescription], [LongDescription], [AdditionalDescription], [Price], [Quantity], [Size], [Color], [CompanyName], [CategoryId], [Sold], [IsCustiomized], [IsActive], [CreatedDate]) VALUES (4, N'HJVHJ', N'gfdgf', N'gdfgfd', N'dgfdhggfhjgf b. j ngfuhjytfrhb ', CAST(1200.00 AS Decimal(18, 2)), 104, N'S,M,L', N'Black,Blue', N'Denim', 2, 0, 0, 1, CAST(N'2024-05-04T10:17:45.180' AS DateTime))
INSERT [dbo].[Product] ([ProductId], [ProductName], [ShortDescription], [LongDescription], [AdditionalDescription], [Price], [Quantity], [Size], [Color], [CompanyName], [CategoryId], [Sold], [IsCustiomized], [IsActive], [CreatedDate]) VALUES (1002, N'asdfghjswfdrg', N'sadfgh', N'sdfghjhwesdfgfdxg', N'qertasfdgthjadghjkdej', CAST(122.00 AS Decimal(18, 2)), 192, N'S,M,L', N'Black,Blue', N'Denim', 1, 0, 0, 1, CAST(N'2024-05-06T19:01:40.143' AS DateTime))
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
INSERT [dbo].[productAddImages] ([ProductId], [image1], [image2], [image3], [image4]) VALUES (1, N'../Images/Product/8dab6635-9b4e-422c-9592-e606d9a4b9fb.jpg', N'../Images/Product/23792ffd-3ae5-4267-8b5d-2eb794e1f19e.jpeg', N'../Images/Product/32d3c7f4-819b-42c7-9a4d-c625cab5f8a1.png', N'../Images/Product/c7dd4d26-3696-4644-a7b2-2507e63392f0.jpg')
INSERT [dbo].[productAddImages] ([ProductId], [image1], [image2], [image3], [image4]) VALUES (2, N'../Images/Product/49aaf8b5-98d3-43a7-b36d-a343b023e055.png', N'../Images/Product/91ffa44e-0ce5-4b63-96aa-501c4238cbae.jpg', N'../Images/Product/85a61a08-8d94-43ca-9c89-cea8a147a212.png', N'../Images/Product/07180d9c-6be4-4f4d-8a27-7cc5763dc440.png')
INSERT [dbo].[productAddImages] ([ProductId], [image1], [image2], [image3], [image4]) VALUES (3, N'../Images/Product/f5e52cc9-5a35-4332-9ad6-dbff0598d60a.jpg', N'../Images/Product/d7d496f4-43cd-46ce-b8db-ac1ad0f3b7e0.jpg', N'../Images/Product/acdf58cd-6bf6-4d60-97b0-5af9c9a8576d.jpg', N'../Images/Product/3a9bc564-7d28-4435-af30-f92e400d46b0.png')
INSERT [dbo].[productAddImages] ([ProductId], [image1], [image2], [image3], [image4]) VALUES (4, N'../Images/Product/d23e3a0a-eea3-4103-8e41-352ef6a98181.jpg', N'../Images/Product/f15b9f17-72cb-45d1-b58f-2b9adc5819b8.png', N'../Images/Product/dd8d0492-a8e0-4c73-8ed9-5d5462ee1d39.jpg', N'../Images/Product/9b0b3a6f-f223-40d9-8b1a-9073483c696e.jpg')
INSERT [dbo].[productAddImages] ([ProductId], [image1], [image2], [image3], [image4]) VALUES (1002, N'../Images/Product/a9d1aae1-0cf2-4501-9882-abf039266f47.jpg', N'../Images/Product/3c995656-d449-4d85-8b53-0acde774c9fa.jpg', N'../Images/Product/717efae9-28cc-4dd8-a88b-fe93018721cb.jpg', N'../Images/Product/8bb62d1c-11cb-4d4a-a73d-fccc58a33d64.png')
GO
SET IDENTITY_INSERT [dbo].[ProductImages] ON 

INSERT [dbo].[ProductImages] ([ImageId], [ImageUrl], [ProductId], [DefaultImage]) VALUES (1, N'../Images/Product/c6c21f7f-93aa-4aba-b337-fa51fcb85730.jpg', 1, 0)
INSERT [dbo].[ProductImages] ([ImageId], [ImageUrl], [ProductId], [DefaultImage]) VALUES (2, N'../Images/Product/f4f0078a-6572-4e1b-b0b5-6e91770795a2.png', 2, 0)
INSERT [dbo].[ProductImages] ([ImageId], [ImageUrl], [ProductId], [DefaultImage]) VALUES (3, N'../Images/Product/1020b938-9665-4463-87cd-74cb136569b0.jpg', 3, 0)
INSERT [dbo].[ProductImages] ([ImageId], [ImageUrl], [ProductId], [DefaultImage]) VALUES (4, N'../Images/Product/9df24067-e19d-47f8-8e8b-ce326874ded4.jpg', 4, 1)
INSERT [dbo].[ProductImages] ([ImageId], [ImageUrl], [ProductId], [DefaultImage]) VALUES (1002, N'../Images/Product/eebc0ea6-7e84-4de6-9bef-d4e43be3e8e5.jpg', 1002, 1)
SET IDENTITY_INSERT [dbo].[ProductImages] OFF
GO
INSERT [dbo].[Roles] ([RoleId], [RoleName]) VALUES (1, N'User')
INSERT [dbo].[Roles] ([RoleId], [RoleName]) VALUES (2, N'Admin')
GO
SET IDENTITY_INSERT [dbo].[ShippingDetails] ON 

INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (1, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (2, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Sargodha', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (3, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Sargodha', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (4, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'asdasdbas', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (5, 35, N'Muhammad', N'Nouman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Sargodha', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (6, 35, N'Muhammad', N'Nouman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'dsadasda', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (7, 35, N'Muhammad', N'Nouman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Faislabad', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (8, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Faislabad', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (9, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (10, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (11, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (12, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Sargodha', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (13, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (1007, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (1008, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (1009, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (1010, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Punjab', N'54810')
INSERT [dbo].[ShippingDetails] ([ShippingDetailID], [UserID], [FirstName], [LastName], [Email], [MobileNo], [AddressLine1], [AddressLine2], [Country], [City], [State], [ZIPCode]) VALUES (1011, 35, N'Nouman', N'Usman', N'noumanmughal0123@gmail.com', N'+923228429291', N'House no.315/A Street No 32 Diggi Muhallah Sadar Bazar Lahore Cantt.', N'', N'Pakistan', N'Lahore', N'Sargodha', N'54810')
SET IDENTITY_INSERT [dbo].[ShippingDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[UserNetworkInfo] ON 

INSERT [dbo].[UserNetworkInfo] ([NetworkInfoId], [UserId], [MacAddress], [IpAddress]) VALUES (20, 35, N'B86B233B1E86', N'::1')
SET IDENTITY_INSERT [dbo].[UserNetworkInfo] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserId], [Name], [Username], [Mobile], [Email], [Adress], [PostCode], [Password], [ImageUrl], [RoleId], [CreatedDate]) VALUES (35, NULL, N'Nouman USMAN', NULL, N'noumanmughal0123@gmail.com', NULL, NULL, N'1242', NULL, 1, CAST(N'2024-05-05T13:41:45.250' AS DateTime))
INSERT [dbo].[Users] ([UserId], [Name], [Username], [Mobile], [Email], [Adress], [PostCode], [Password], [ImageUrl], [RoleId], [CreatedDate]) VALUES (36, NULL, N'Nouman', N'0322842921', N'noumanusman.uet@gmail.com', NULL, NULL, N'1122', NULL, 2, CAST(N'2024-05-10T13:41:45.250' AS DateTime))
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Users__536C85E45FBBD8B3]    Script Date: 5/10/2024 9:31:33 AM ******/
ALTER TABLE [dbo].[Users] ADD UNIQUE NONCLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT ((0)) FOR [IsCancel]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[cartdetails]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[productAddImages]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[ProductImages]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductReview]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductReview]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ShippingDetails]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[SubCategory]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserNetworkInfo]  WITH CHECK ADD  CONSTRAINT [FK_UserNetworkInfo_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserNetworkInfo] CHECK CONSTRAINT [FK_UserNetworkInfo_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Wishlist]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Wishlist]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
/****** Object:  StoredProcedure [dbo].[cart_crud]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cart_crud]
	-- Add the parameters for the stored procedure here
	@Action VARCHAR(10),
	@ProductId int = Null,
	@UserId int = Null,
	@Quantity int = Null,
	@Color VARCHAR(10) = Null,
	@Size VARCHAR(10) = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if (@Action = 'INSERT')
	BEGIN
	DECLARE @AvailableQuantity INT;
	SELECT @AvailableQuantity = Quantity from Product WHERE ProductId = @ProductId;
	if (@AvailableQuantity > @Quantity)
	BEGIN
	Insert into Cart(ProductId,UserId,Quantity,CreatedDate)
	Values (@ProductId,@UserId,@Quantity,GETDATE())
	INSERT INTO cartdetails(UserId, color,size,cartID) VALUES(@UserId,@Color,@Size, (Select MAX(CartId) from Cart))
	END
	ELSE
	BEGIN 
		Insert into Cart(ProductId,UserId,Quantity,CreatedDate)
	Values (@ProductId,@UserId,@AvailableQuantity,GETDATE())
	INSERT INTO cartdetails(UserId, color,size,cartID) VALUES(@UserId,@Color,@Size, (Select MAX(CartId) from Cart))
	END
	END
    if (@Action = 'GETALL')
	BEGIN
	SELECT 
    Cart.*,
    Product.Price,
    Product.ProductName,
    ProductImages.ImageUrl,
    cartdetails.color,
    cartdetails.size
FROM 
    Cart
INNER JOIN 
    Product ON Cart.ProductId = Product.ProductId
INNER JOIN 
    ProductImages ON Cart.ProductId = ProductImages.ProductId
INNER JOIN (
    SELECT 
        UserId,
        MIN(color) AS color,
        MIN(size) AS size
    FROM 
        cartdetails
    GROUP BY 
        UserId
) AS cartdetails ON Cart.UserId = cartdetails.UserId
WHERE 
    Cart.UserId = @UserId
	END
	IF (@Action = 'GETCARTITEMCOUNT')
    BEGIN
        SELECT COUNT(*) AS CartItemCount
        FROM Cart
        WHERE UserId = @UserId;
    END
	if (@Action = 'DELETECART')
	BEGIN
		DELETE FROM CartDetails
        WHERE UserId = @UserId AND cartID = (Select CartId from Cart where UserId = @UserId AND ProductId = @ProductId )
	DELETE FROM Cart
    WHERE UserId = @UserId
    AND ProductId = @ProductId;
	END
END
GO
/****** Object:  StoredProcedure [dbo].[cart_op]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cart_op] 
	@Action VARCHAR(10),
	@ProductId int = Null,
	@UserId int = Null,
	@Quantity int = Null,
	@UpdatedQuantity INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
if(@Action = 'ADDCART')
	BEGIN
	DECLARE @AvailableQuantity INT;
	SELECT @AvailableQuantity = Quantity from Product WHERE ProductId = @ProductId;
IF @AvailableQuantity > (SELECT Quantity
            FROM Cart
            WHERE UserId = @UserId
            AND ProductId = @ProductId)
        BEGIN
            -- Update quantity in the Cart table
            UPDATE Cart
            SET Quantity = Quantity + 1
            WHERE UserId = @UserId
            AND ProductId = @ProductId;

            -- Select updated quantity
            SELECT @UpdatedQuantity = Quantity
            FROM Cart
            WHERE UserId = @UserId
            AND ProductId = @ProductId;
        END
        ELSE
        BEGIN
            SET @UpdatedQuantity = (SELECT Quantity FROM Cart WHERE UserId = @UserId AND ProductId = @ProductId);
        END
	END
	if(@Action = 'SUBCART')
	BEGIN
UPDATE Cart
    SET Quantity = CASE WHEN Quantity > 1 THEN Quantity - 1 ELSE 1 END
    WHERE UserId = @UserId
    AND ProductId = @ProductId;
	SELECT @UpdatedQuantity = Quantity
    FROM Cart
    WHERE UserId = @UserId
    AND ProductId = @ProductId;
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[Category_Crud]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Category_Crud]
	@Action VARCHAR(15),
	@CategoryId INT = NULL,
	@CategoryName VARCHAR(100) = NULL,
	@CategoryImageUrl VARCHAR(MAX) = NULL,
	@IsActive BIT = False
	
AS
BEGIN
	SET NOCOUNT ON;

    -- Get all category
	if (@Action = 'GETALL')
	BEGIN
		Select * From Category
	END

	if(@Action = 'GETBYID')
	BEGIN
	Select * from Category c
	Where c.CategoryID = @CategoryId;
	END

	-- Insert category
	if (@Action = 'INSERT')
	BEGIN
	INSERT  INTO Category (CategoryName,CategoryImageUrl,IsActive,CreatedDate)
	VALUES(@CategoryName, @CategoryImageUrl,@IsActive,GETDATE())
	END
	-- Update category
	if (@Action = 'UPDATE')
	BEGIN
	DECLARE @UPDATE_IMAGE VARCHAR(20)
	SELECT @UPDATE_IMAGE = (CASE WHEN @CategoryImageUrl is NULL THEN 'NO' ELSE 'YES' END)
	if (@UPDATE_IMAGE = 'NO')
		BEGIN
			UPDATE   Category 
			SET CategoryName = @CategoryName, IsActive = @IsActive
			WHERE CategoryID = @CategoryId
		END
	ELSE
		BEGIN
			UPDATE   Category 
			SET CategoryName = @CategoryName,@CategoryImageUrl= @CategoryImageUrl, IsActive = @IsActive
			WHERE CategoryID = @CategoryId
		END
	END
	-- Delete Category 
	if (@Action = 'DELETE')
	BEGIN
		DELETE From Category
		WHERE CategoryID = @CategoryId
	END
	-- Get Active Category From Category

	if (@Action = 'ACTIVECATEGORY')
	BEGIN
		SELECT * From Category c
		WHERE c.IsActive = 1 ORDER BY c.CategoryName
	END
END
GO
/****** Object:  StoredProcedure [dbo].[PicturesCrud]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PicturesCrud] 
	@Action VARCHAR(10) = NULL,
	@ImageId INT = NULL,
	@ImageUrl VARCHAR(MAX) = NULL,
	@ProductId INT = NULL,
	@DefaultImageUrl bit = false,
	@Image1 VARCHAR(MAX) = NULL,
	@Image2 VARCHAR(MAX) = NULL,
	@Image3 VARCHAR(MAX) = NULL,
	@Image4 VARCHAR(MAX) = NULL
	
AS
BEGIN
	SET NOCOUNT ON;
	if(@Action = 'INSERT')
	BEGIN
	INSERT INTO ProductImages (ImageUrl, ProductId, DefaultImage)
VALUES (@ImageUrl, @ProductId, @DefaultImageUrl)
	END
	if(@Action = 'GETBYID')
	BEGIN
	Select * from ProductImages
	Where ProductId = @ProductId
	END
	if(@Action = 'GETALL')
	BEGIN
	Select * from ProductImages
	END
	If (@Action = 'DELETE')
	BEGIN
	Delete from ProductImages
	Where ProductId = @ProductId
	END
	if(@Action = 'CHECK')
	BEGIN
	SELECT ProductId FROM Product WHERE @ProductId = ProductId 
	END
	IF(@Action = 'UPDATE')
BEGIN
    UPDATE ProductImages
    SET 
	ImageUrl = @ImageUrl,
        DefaultImage = @DefaultImageUrl
    WHERE ProductId = @ProductId
END
END
GO
/****** Object:  StoredProcedure [dbo].[Product_Crud]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Product_Crud]
	@Action VARCHAR(15),
	@ProductID INT = NULL,
	@ProductName VARCHAR(100) = NULL,
	@ShortDescription VARCHAR(100) = NULL,
	@LongDescription VARCHAR(MAX) = NULL,
	@AdditionalDescription VARCHAR(100) = NULL,
	@Price INT = NULL,
	@Quantity INT = NULL,
	@Size VARCHAR(5) = NULL,
	@Color VARCHAR(10) = NULL,
	@CompanyName VARCHAR(20) = NULL,
	@CategoryId INT = NULL,
	@Sold BIT = False,
	@IsCustomized BIT = False,
	@IsActive BIT = False
AS
BEGIN
	SET NOCOUNT ON;
	if (@Action = 'GETALL')
	BEGIN
		Select * From Product
	END
	if(@Action = 'GETBYID')
	BEGIN
	SELECT 
        p.ProductID,
        p.ProductName,
        p.ShortDescription,
        p.LongDescription,
        p.AdditionalDescription,
        p.Price,
        p.Quantity,
        p.Size,
        p.Color,
        p.CompanyName,
        p.CategoryId,
        p.Sold,
        p.IsCustiomized,
        p.IsActive,
        p.CreatedDate
    FROM 
        Product p
    WHERE 
        p.ProductID = @ProductID;
	END
	if (@Action = 'INSERT')
	BEGIN
	INSERT  INTO Product(ProductName,ShortDescription,LongDescription,AdditionalDescription,Price,Quantity,Size,Color,CompanyName,CategoryId,Sold,IsCustiomized,IsActive,CreatedDate)
	VALUES(@ProductName, @ShortDescription,@LongDescription,@AdditionalDescription,@Price,@Quantity,@Size,@Color,@CompanyName,@CategoryId,@Sold,@IsCustomized,@IsActive,GETDATE())
	END
	if (@Action = 'UPDATE')
	BEGIN
			UPDATE Product
    SET 
        ProductName = ISNULL(@ProductName, ProductName),
        ShortDescription = @ShortDescription,
        LongDescription = @LongDescription,
        AdditionalDescription = @AdditionalDescription,
        Price = @Price,
        Quantity = @Quantity,
        Size = @Size,
        Color = @Color,
        CompanyName = @CompanyName,
        CategoryId = @CategoryId,
        Sold = @Sold,
        IsCustiomized = @IsCustomized,
        IsActive = @IsActive
    WHERE ProductId = @ProductID;
	END
	if (@Action = 'DELETE')
	BEGIN
		DELETE From Product 
		WHERE ProductId = @ProductID
	END
	If (@Action = 'GETPRODUCTID')
	BEGIN
		SELECT TOP 1 ProductId FROM Product ORDER BY ProductId DESC
	END
	if (@Action = 'ACTIVEPRODUCT')
	BEGIN
		SELECT * From Product c
		WHERE c.IsActive = 1 AND c.Quantity > 0
		ORDER BY c.ProductName
	END
	if (@Action = 'GETWITHIMAGE')
	BEGIN
	SELECT 
            p.ProductID,
            p.ProductName,
            p.ShortDescription,
            p.LongDescription,
            p.AdditionalDescription,
            p.Price,
            p.Quantity,
            p.Size,
            p.Color,
            p.CompanyName,
            p.CategoryId,
            p.Sold,
            p.IsCustiomized,
            p.IsActive,
            p.CreatedDate,
            pi.ImageUrl -- Include product image URL
        FROM 
            Product p
        LEFT JOIN 
            ProductImages pi ON p.ProductID = pi.ProductID -- Join with ProductImages table
        WHERE 
            p.IsActive = 1 AND p.CategoryId = @CategoryId AND p.Quantity>0 -- Assuming you want only active products
        ORDER BY 
            p.ProductName;

	END
	if (@Action = 'GETPRODUCTDETAILS')
	BEGIN
	SELECT *
        FROM Product         WHERE 
            IsActive = 1 AND ProductId = @ProductID 
        ORDER BY 
            ProductName;

	END
    END
GO
/****** Object:  StoredProcedure [dbo].[shipping_Crud]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[shipping_Crud]
@Action NVARCHAR(10),
    @ShippingDetailID INT = NULL,
    @UserID INT = NULL,
    @FirstName NVARCHAR(50) = NULL,
    @LastName NVARCHAR(50) = NULL,
    @Email NVARCHAR(100) = NULL,
    @MobileNo NVARCHAR(20) = NULL,
    @AddressLine1 NVARCHAR(100) = NULL,
    @AddressLine2 NVARCHAR(100) = NULL,
    @Country NVARCHAR(50) = NULL,
    @City NVARCHAR(50) = NULL,
    @State NVARCHAR(50) = NULL,
    @ZIPCode NVARCHAR(20) = NULL,
	@OrderNo NVARCHAR(30) = NULL,
	@Status NVARCHAR(20) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @Action = 'INSERT'
    BEGIN
	BEGIN TRANSACTION;
        INSERT INTO ShippingDetails (UserID, FirstName, LastName, Email, MobileNo, AddressLine1, AddressLine2, Country, City, State, ZIPCode)
        VALUES (@UserID, @FirstName, @LastName, @Email, @MobileNo, @AddressLine1, @AddressLine2, @Country, @City, @State, @ZIPCode);
		DECLARE @CartID INT;		
    -- Insert order details
    INSERT INTO Orders (OrderNo, ProductId, Quantity, UserId, Status, OrderDate, IsCancel)
        SELECT @OrderNo, ProductId, Quantity, @UserId, 'Pending', GETDATE(), 0
        FROM Cart
        WHERE UserId = @UserID;
    SELECT @CartID = CartID FROM Cart WHERE UserId = @UserId;
    UPDATE Product
    SET Quantity = Quantity - (SELECT Quantity FROM Cart WHERE CartId = @CartID AND ProductId = Product.ProductID)
    WHERE ProductID IN (SELECT ProductId FROM Cart WHERE CartID = @CartID);
    DELETE FROM cartdetails WHERE UserId = @UserID;
	DELETE FROM Cart WHERE UserId = @UserId;
	COMMIT TRANSACTION;		
    END
    ELSE IF @Action = 'UPDATE'
    BEGIN
        UPDATE Orders
        SET 
            Status = @Status
        WHERE OrderNo = @OrderNo;
    END
    ELSE IF @Action = 'DELETE'
    BEGIN
        DELETE FROM ShippingDetails
        WHERE ShippingDetailID = @ShippingDetailID;
    END
    ELSE IF @Action = 'SELECT'
    BEGIN
        SELECT * FROM ShippingDetails WHERE ShippingDetailID = @ShippingDetailID;
    END
	ELSE IF @Action = 'GETALL'
BEGIN
    SELECT O.*, P.ProductName
    FROM Orders AS O
    INNER JOIN Product AS P ON O.ProductId = P.ProductId
    WHERE UserId = @UserID;
END
ELSE IF @Action = 'LOAD'
BEGIN
SELECT O.*, P.ProductName, U.Username, AddressLine1
    FROM Orders AS O
    INNER JOIN Product AS P ON O.ProductId = P.ProductId
    INNER JOIN Users AS U ON O.UserId = U.UserId
    INNER JOIN (
        SELECT UserId, MIN(AddressLine1) AS AddressLine1
        FROM ShippingDetails
        GROUP BY UserId
    ) AS SD ON O.UserId = SD.UserId
END


END
GO
/****** Object:  StoredProcedure [dbo].[signup]    Script Date: 5/10/2024 9:31:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[signup] 
	@Action VARCHAR(10) = Null,
	@Username VARCHAR(20) = Null,
	@password VARCHAR(50) = NULL,
	@email VARCHAR(50) = NULL,
	@Roleid int = NULL,
	@MacAddress NVARCHAR(50),
    @IpAddress NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN
        -- Insert into Users table
        INSERT INTO Users (Username, Email, Password, RoleId,CreatedDate)
        VALUES (@Username, @Email, @Password, @RoleId,GETDATE());
        -- Get the ID of the inserted user
        DECLARE @UserId INT;
        SET @UserId = SCOPE_IDENTITY(); 
        -- Insert into UserNetworkInfo table
        INSERT INTO UserNetworkInfo (UserId, MacAddress, IpAddress)
        VALUES (@UserId, @MacAddress, @IpAddress);
		SET @RoleId = SCOPE_IDENTITY(); 
    END
    -- Insert statements for procedure here
END
GO
USE [master]
GO
ALTER DATABASE [JSM] SET  READ_WRITE 
GO
