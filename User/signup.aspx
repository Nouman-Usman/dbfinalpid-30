﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="signup.aspx.cs" Inherits="JSM.User.signup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-6 p-5">
                <div class="md-4">
                    <asp:Label ID="signUplblmsg" runat="server"></asp:Label>
                </div>
                <h2>Sign Up</h2>
                <div class="form-group">
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder="Name"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required." CssClass="text-danger" ValidationGroup="SignUpValidation"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required." CssClass="text-danger" ValidationGroup="SignUpValidation"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid email format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="text-danger" ValidationGroup="SignUpValidation"></asp:RegularExpressionValidator>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password is required." CssClass="text-danger" ValidationGroup="SignUpValidation"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="Confirm Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="Confirm Password is required." CssClass="text-danger" ValidationGroup="SignUpValidation"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword" ErrorMessage="Passwords do not match." CssClass="text-danger" ValidationGroup="SignUpValidation"></asp:CompareValidator>
                </div>
                <asp:Button ID="btnSignUp" runat="server" Text="Sign Up" CssClass="btn btn-primary" ValidationGroup="SignUpValidation" OnClick="btnSignUp_Click" />
            </div>
            <div class="col-md-6 p-5">
                <div class="md-4">
                    <asp:Label ID="signinLabel" runat="server"></asp:Label>
                </div>
                <h2>Sign In</h2>
                <div class="form-group">
                    <asp:TextBox ID="txtEmailSignIn" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmailSignIn" runat="server" ControlToValidate="txtEmailSignIn" ErrorMessage="Email is required." CssClass="text-danger" ValidationGroup="SignInValidation"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtPasswordSignIn" runat="server" CssClass="form-control" TextMode="Password" placeholder="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPasswordSignIn" runat="server" ControlToValidate="txtPasswordSignIn" ErrorMessage="Password is required." CssClass="text-danger" ValidationGroup="SignInValidation"></asp:RequiredFieldValidator>
                </div>
                <asp:Button ID="btnSignIn" runat="server" Text="Sign In" CssClass="btn btn-primary" ValidationGroup="SignInValidation" OnClick="btnSignIn_Click" />
            </div>
        </div>
    </div>

</asp:Content>
