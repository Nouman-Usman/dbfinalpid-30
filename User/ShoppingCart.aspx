﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="JSM.User.ShoppingCart" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function updateCart(delta) {
            var quantity = parseInt(document.getElementById('txtQuantity').value) + delta;
            if (quantity >= 0) {
                // AJAX request to update the cart
                var productId = getProductId(); // Implement this function to get the product ID
                var userId = getUserId(); // Implement this function to get the user ID
                var data = {
                    productId: productId,
                    userId: userId,
                    quantity: quantity
                };

                $.ajax({
                    type: "POST",
                    url: "ShoppingCart.aspx/UpdateCartQuantity",
                    data: data,
                    success: function (response) {
                        // Update the displayed quantity on success
                        document.getElementById('txtQuantity').value = quantity;
                        // Optionally, update other UI elements or display a success message
                    },
                    error: function (xhr, status, error) {
                        // Handle errors
                        console.error(xhr.responseText);
                    }
                });
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page Header Start -->
    <div class="container-fluid bg-secondary mb-5">
        <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 300px">
            <h1 class="font-weight-semi-bold text-uppercase mb-3">Shopping Cart</h1>
            <div class="d-inline-flex">
                <p class="m-0"><a href="Default.aspx">Home</a></p>
                <p class="m-0 px-2">-</p>
                <p class="m-0">Shopping Cart</p>
            </div>
        </div>
    </div>
    <!-- Page Header End -->
    <!-- Page Header End -->
    <!-- Cart Start -->
    <div class="container-fluid pt-5">
        <div class="row px-xl-5">
            <div class="col-lg-8 table-responsive mb-5">
                <asp:Repeater ID="Repeater1" runat="server">
                    <HeaderTemplate>
                        <table class="table table-bordered text-center mb-0">
                            <thead class="bg-secondary text-dark">
                                <tr>
                                    <th>Products</th>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="align-middle">
                                <img src='<%# Eval("ImageUrl") %>' alt="" style="width: 50px;">
                                <%# Eval("ProductName") %>
                            </td>
                            <td class="align-middle"><%# Eval("size") %></td>
                            <td class="align-middle"><%# Eval("color") %></td>
                            <td class="align-middle"><%# Convert.ToInt32(Eval("Price")) %> RS</td>
                            <td class="align-middle">
                                <div class="input-group quantity mx-auto" style="width: 100px;">
                                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-sm btn-primary btn-minus" Text="-" OnClick="Button1_Click" CommandArgument='<%# Eval("ProductId") %>' />
                                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control form-control-sm bg-secondary text-center" Text='<%# Eval("Quantity") %>'></asp:TextBox>
                                    <div class="input-group-btn">
                                        <asp:Button ID="btnPlus" runat="server" CssClass="btn btn-sm btn-primary btn-plus" Text="+" OnClick="btnPlus_Click" CommandArgument='<%# Eval("ProductId") %>' />
                                    </div>
                                </div>
                            </td>
                            <td class="align-middle"><%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) %> RS</td>
                            <td class="align-middle">
                                <asp:Button ID="btnRemove" runat="server" CssClass="btn btn-sm btn-primary" Text="Remove" OnClick="btnRemove_Click" CommandArgument='<%# Eval("ProductId") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="col-lg-4">
                <%--<form class="mb-5" action="">
                    <div class="input-group">
                        <input type="text" class="form-control p-4" placeholder="Coupon Code">
                        <div class="input-group-append">
                            <button class="btn btn-primary">Apply Coupon</button>
                        </div>
                    </div>
                </form>--%>
                <div class="card border-secondary mb-5">
                    <div class="card-header bg-secondary border-0">
                        <h4 class="font-weight-semi-bold m-0">Cart Summary</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between mb-3 pt-1">
                            <h6 class="font-weight-medium">Subtotal</h6>
                            <asp:Label ID="lblSubtotal" runat="server" CssClass="font-weight-medium"></asp:Label>
                            <%--<h6 class="font-weight-medium" ID="lblSubtotal1" runat="server"></h6>--%>
                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">Shipping</h6>
                            <asp:Label ID="lblShipping" runat="server" CssClass="font-weight-medium"></asp:Label>
                            <%--<h6 class="font-weight-medium" id="lblShipping" runat="server"></h6>--%>
                        </div>
                    </div>
                    <div class="card-footer border-secondary bg-transparent">
                        <div class="d-flex justify-content-between mt-2">
                            <h5 class="font-weight-bold">Total</h5>
                            <asp:Label ID="lblTotal" runat="server" CssClass="font-weight-bold"></asp:Label>
                            <%--<h5 class="font-weight-bold" id="lblTotal" runat="server"></h5>--%>
                        </div>
                        <asp:Button ID="checkoutBtn" runat="server" CssClass="btn btn-block btn-primary my-3 py-3" Text="Proceed To Checkout" OnClick="checkoutBtn_Click" />
                        <%--<button class="btn btn-block btn-primary my-3 py-3">Proceed To Checkout</button>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cart End -->
</asp:Content>
