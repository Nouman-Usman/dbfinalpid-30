﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.User
{
    public partial class checkout : System.Web.UI.Page
    {
        int quantity = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                // Initialize form fields
                firstName.Text = "";
                lastName.Text = "";
                email.Text = "";
                txtMobileNo.Text = "";
                txtAddressLine1.Text = "";
                txtAddressLine2.Text = "";
                txtCity.Text = "";
                txtState.Text = "";
                // Add more fields as needed

                DataTable checkoutDetails = getCheckOutDetails();
                int subtotal = 0;
                string productDetailsHtml = "";

                foreach (DataRow row in checkoutDetails.Rows)
                {
                    string productName = row["ProductName"].ToString();
                    int unitPrice = Convert.ToInt32(row["Price"]);
                    quantity = Convert.ToInt32(row["Quantity"]);
                    int totalPrice = unitPrice * quantity;

                    // Add product details to the HTML string
                    productDetailsHtml += $"<div class='d-flex justify-content-between'><p>{productName}</p><p>{unitPrice} PKR </p></div>";
                    productDetailsHtml += $"<div class='d-flex justify-content-between'><p>Quantity: {quantity}</p></div>";

                    // Add the total price to the subtotal
                    subtotal += totalPrice;
                }

                // Calculate shipping (example: fixed shipping rate)
                decimal shipping = 0;
                if (subtotal < 3000 & subtotal > 0)
                {
                    shipping = 200;
                }

                // Calculate total
                int total = Convert.ToInt32(subtotal + shipping);

                // Set the text of labels
                lblProductDetails.Text = productDetailsHtml;
                lblSubtotal.Text = $"{subtotal} PKR";
                lblShipping.Text = $"{shipping} PKR";
                lblTotal.Text = $"{total} PKR";
            }
        }
        public DataTable getCheckOutDetails()
        {
            DataTable dataTable = new DataTable();
            int UserId = Convert.ToInt32(Request.QueryString["UserId"]);            
            using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("cart_crud", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Action", "GETALL");
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(dataTable);
                    }
                }
            }
            return dataTable;
        }

        protected void orderBtn_Click(object sender, EventArgs e)
        {
            string billingFirstName = firstName.Text;
            string billingLastName = lastName.Text;
            string billingEmail = email.Text;
            string billingMobileNo = txtMobileNo.Text;
            string billingAddressLine1 = txtAddressLine1.Text;
            string billingAddressLine2 = txtAddressLine2.Text;
            string billingCountry = ddlCountry.SelectedValue;
            string billingCity = txtCity.Text;
            string billingState = txtState.Text;
            string billingZIPCode = txtZIPCode.Text;
            int userId = Convert.ToInt32(Request.QueryString["UserId"]);
            string rand = new Random().Next(1000, 9999).ToString();
            string orderNo = "ORD" + userId.ToString() + "-" + billingZIPCode.ToString() + rand;
            try
            {


                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand("shipping_Crud", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Action", "INSERT");
                        cmd.Parameters.AddWithValue("@UserID", userId);
                        cmd.Parameters.AddWithValue("@FirstName", billingFirstName);
                        cmd.Parameters.AddWithValue("@LastName", billingLastName);
                        cmd.Parameters.AddWithValue("@Email", billingEmail);
                        cmd.Parameters.AddWithValue("@MobileNo", billingMobileNo);
                        cmd.Parameters.AddWithValue("@AddressLine1", billingAddressLine1);
                        cmd.Parameters.AddWithValue("@AddressLine2", billingAddressLine2);
                        cmd.Parameters.AddWithValue("@Country", billingCountry);
                        cmd.Parameters.AddWithValue("@City", billingCity);
                        cmd.Parameters.AddWithValue("@State", billingState);
                        cmd.Parameters.AddWithValue("@ZIPCode", billingZIPCode);
                        cmd.Parameters.AddWithValue("@OrderNo", orderNo);
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                Response.Redirect("ThankYou.aspx?&UserId=" + userId);    
            }
            catch (Exception ax)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "formatError", "alert('There is some error occurs during order.');", true);
            }



        }
    }
}