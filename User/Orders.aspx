﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="JSM.User.Orders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="card">
    <div class="card-body">
        <h4 class="card-title">Order Details</h4>
        <hr />
        <div class="table-responsive">
            <asp:Repeater ID="pdTable" runat="server">
                <HeaderTemplate>
                    <table class="table data-table-export table-hover nowrap">
                        <thead>
                            <tr>
                                <th class="table-plus">Product Name</th>
                                <th>OrderNo</th>
                                <th>Order Date</th>
                                <th>Quantity</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="table-plus"><%# Eval("ProductName") %></td>
                        <td class="table-plus"><%# Eval("OrderNo") %></td>
                        <td><%# Eval("OrderDate") %></td>
                        <td><%# Eval("Quantity") %></td>
                        <td>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' CssClass="badge badge-primary"></asp:Label>
                        </td>
                        <td>
                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CssClass="badge badge-primary"
                                CommandArgument='<%# Eval("OrderNo") %>' CommandName="edit" CausesValidation="false">
                                <i class="fas fa-edit"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lblDelete" runat="server" Text="Delete" CssClass="badge badge-danger"
                                CommandArgument='<%# Eval("OrderNo") %>' CommandName="delete" CausesValidation="false">
                                <i class="fas fa-trash-alt"></i>
                            </asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                        </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>

</asp:Content>
