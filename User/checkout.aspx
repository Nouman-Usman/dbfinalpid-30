﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="checkout.aspx.cs" Inherits="JSM.User.checkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function toggleRadioButton(radioButton) {
            // Check if the radio button is currently checked
            var isChecked = radioButton.checked;

            // If it's checked, uncheck it
            if (isChecked) {
                radioButton.checked = false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page Header Start -->
    <div class="container-fluid bg-secondary mb-5">
        <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 300px">
            <h1 class="font-weight-semi-bold text-uppercase mb-3">Checkout</h1>
            <div class="d-inline-flex">
                <p class="m-0"><a href="Default.aspx">Home</a></p>
                <p class="m-0 px-2">-</p>
                <p class="m-0">Checkout</p>
            </div>
        </div>
    </div>

    <!-- Checkout Start -->
    <div class="container-fluid pt-5">
        <div class="row px-xl-5">
            <div class="col-lg-8">
                <div class="mb-4">
                    <h4 class="font-weight-semi-bold mb-4">Shipping Address</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <asp:Label runat="server" Text="First Name"></asp:Label>
                            <asp:TextBox ID="firstName" runat="server" CssClass="form-control" placeholder="Nouman"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="firstName" ErrorMessage="Please enter First Name" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label Text="Last Name" runat="server"> </asp:Label>
                            <asp:TextBox ID="lastName" runat="server" CssClass="form-control" placeholder="Usman"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="lastName" ErrorMessage="Please enter Last Name" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label Text="Email" runat="server"> </asp:Label>
                            <asp:TextBox ID="email" runat="server" CssClass="form-control" placeholder="xyz@gmail.com"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="email" ErrorMessage="Invalid Email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="text-danger" ValidationGroup="Billing"></asp:RegularExpressionValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="lblMobileNo" runat="server" Text="Mobile No"></asp:Label>
                            <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" placeholder="+123 456 789"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvMobileNo" runat="server" ControlToValidate="txtMobileNo" ErrorMessage="Please enter Mobile No" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="lblAddressLine1" runat="server" Text="Address Line 1"></asp:Label>
                            <asp:TextBox ID="txtAddressLine1" runat="server" CssClass="form-control" placeholder="123 Street"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvAddressLine1" runat="server" ControlToValidate="txtAddressLine1" ErrorMessage="Please enter Address Line 1" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="lblAddressLine2" runat="server" Text="Address Line 2"></asp:Label>
                            <asp:TextBox ID="txtAddressLine2" runat="server" CssClass="form-control" placeholder="123 Street"></asp:TextBox>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="custom-select">
                                <asp:ListItem Text="Pakistan" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="United States"></asp:ListItem>
                                <asp:ListItem Text="Italy"></asp:ListItem>
                                <asp:ListItem Text="Canada"></asp:ListItem>
                                <asp:ListItem Text="Saudi Arabia"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" placeholder="Lahore"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="Please enter City" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>
                            <asp:TextBox ID="txtState" runat="server" CssClass="form-control" placeholder="Punjab"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="txtState" ErrorMessage="Please enter State" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="lblZIPCode" runat="server" Text="ZIP Code"></asp:Label>
                            <asp:TextBox ID="txtZIPCode" runat="server" CssClass="form-control" placeholder="54810"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvZIPCode" runat="server" ControlToValidate="txtZIPCode" ErrorMessage="Please enter ZIP Code" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <%--<div class="col-md-12 form-group">
                            <div class="custom-control custom-checkbox">
                                <asp:RadioButtonList ID="rblOptions" runat="server">
                                    <asp:ListItem Text="Create Account" Value="CreateAccount" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Ship to another Location" Value="ShipToLocation" data-toggle="collapse" data-target="#shipping-address" ></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RadioButton ID="chkCreateAccount" runat="server" Text="Create Account" Checked="true"></asp:RadioButton>
                            </div>
                        </div>   --%>                     
                    </div>
                </div>
<%--                <div class="collapse mb-4" id="shipping-address">
                    <h4 class="font-weight-semi-bold mb-4">Shipping Address</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <asp:Label runat="server" Text="First Name"></asp:Label>
                            <asp:TextBox ID="shipFirstName" runat="server" CssClass="form-control" placeholder="Nouman"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="shipFirstName" ErrorMessage="Please enter First Name" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label Text="Last Name" runat="server"> </asp:Label>
                            <asp:TextBox ID="shipLastName" runat="server" CssClass="form-control" placeholder="Usman"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="shipLastName" ErrorMessage="Please enter Last Name" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label Text="Email" runat="server"> </asp:Label>
                            <asp:TextBox ID="ShipEmail" runat="server" CssClass="form-control" placeholder="xyz@gmail.com"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="ShipEmail" ErrorMessage="Invalid Email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="text-danger" ValidationGroup="Billing"></asp:RegularExpressionValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="Label1" runat="server" Text="Mobile No"></asp:Label>
                            <asp:TextBox ID="shipMobileNo" runat="server" CssClass="form-control" placeholder="+123 456 789"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="shipMobileNo" ErrorMessage="Please enter Mobile No" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="Label2" runat="server" Text="Address Line 1"></asp:Label>
                            <asp:TextBox ID="shipAdress1" runat="server" CssClass="form-control" placeholder="123 Street"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="shipAdress1" ErrorMessage="Please enter Address Line 1" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="shipAdress2" runat="server" Text="Address Line 2"></asp:Label>
                            <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control" placeholder="123 Street"></asp:TextBox>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="shipCountry" runat="server" Text="Country"></asp:Label>
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="custom-select">
                                <asp:ListItem Text="Pakistan" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="United States"></asp:ListItem>
                                <asp:ListItem Text="Italy"></asp:ListItem>
                                <asp:ListItem Text="Canada"></asp:ListItem>
                                <asp:ListItem Text="Saudi Arabia"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="Label5" runat="server" Text="City"></asp:Label>
                            <asp:TextBox ID="shipCity" runat="server" CssClass="form-control" placeholder="Lahore"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="shipCity" ErrorMessage="Please enter City" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="Label6" runat="server" Text="State"></asp:Label>
                            <asp:TextBox ID="shipState" runat="server" CssClass="form-control" placeholder="Punjab"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="shipState" ErrorMessage="Please enter State" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 form-group">
                            <asp:Label ID="Label7" runat="server" Text="ZIP Code"></asp:Label>
                            <asp:TextBox ID="shipZip" runat="server" CssClass="form-control" placeholder="54810"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="shipZip" ErrorMessage="Please enter ZIP Code" CssClass="text-danger" ValidationGroup="Billing"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>--%>
            </div>
            <div class="col-lg-4">
                <div class="card border-secondary mb-5">
                    <div class="card-header bg-secondary border-0">
                        <h4 class="font-weight-semi-bold m-0">Order Total</h4>
                    </div>
                    <div class="card-body">
                        <h5 class="font-weight-medium mb-3">Products</h5>
                        <asp:Label ID="lblProductDetails" runat="server" CssClass="font-weight-medium mb-3" />
                        <hr class="mt-0">
                        <div class="d-flex justify-content-between mb-3 pt-1">
                            <h6 class="font-weight-medium">Subtotal</h6>
                            <asp:Label ID="lblSubtotal" runat="server" CssClass="font-weight-medium" />
                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">Delivery Charges</h6>
                            <asp:Label ID="lblShipping" runat="server" CssClass="font-weight-medium" />
                        </div>
                    </div>
                    <div class="card-footer border-secondary bg-transparent">
                        <div class="d-flex justify-content-between mt-2">
                            <h5 class="font-weight-bold">Total</h5>
                            <asp:Label ID="lblTotal" runat="server" CssClass="font-weight-bold" />
                        </div>
                    </div>
                </div>


                <div class="card border-secondary mb-5">
                    <div class="card-header bg-secondary border-0">
                        <h4 class="font-weight-semi-bold m-0">Payment</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="payment" id="paypal">
                                <label class="custom-control-label" for="paypal">Cash On Delivery</label>
                            </div>
                        </div>
                        <%--<div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="payment" id="directcheck">
                                <label class="custom-control-label" for="directcheck">Direct Check</label>
                            </div>
                        </div>--%>
                        <div class="">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="payment" id="banktransfer">
                                <label class="custom-control-label" for="banktransfer">Bank Transfer</label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border-secondary bg-transparent">
                        <asp:Button ID="orderBtn" runat="server" CssClass="btn btn-lg btn-block btn-primary font-weight-bold my-3 py-3" Text="Place Order" OnClick="orderBtn_Click" ValidationGroup="Billing" />
                        <%--<button class="btn btn-lg btn-block btn-primary font-weight-bold my-3 py-3">Place Order</button>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Checkout End -->
</asp:Content>
