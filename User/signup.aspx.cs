﻿using JSM.Admin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.User
{
    public partial class signup : System.Web.UI.Page
    {
        string previousPageUrl = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            previousPageUrl = Request.QueryString["PreviousUrl"].ToString();
            string macAddress = Utils.getMacAdress();
            string ipAddress = Utils.GetIPAddress();
            // Check if there is a user with the same MAC address or IP address
            Session["PreviousPageUrl"] = previousPageUrl;
            (bool userFound, int userid) = CheckUserByMacOrIpAddress(macAddress, ipAddress);

            // If a user is found, sign them in automatically
            if (userFound)
            {
                SignInUserAutomatically(userid);
            }
        }

        protected void btnSignUp_Click(object sender, EventArgs e)
        {
            string macadress = Utils.getMacAdress();
            string ipAdress = Utils.GetIPAddress();
            string username = txtName.Text;
            int roleId = 1;
            int userId = 0;
            string password = txtPassword.Text;
            string email = txtEmail.Text;
            string confirmPassword = txtConfirmPassword.Text;
            try
            {
                using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
                {
                    connection.Open();
                    try
                    {
                        // Get the maximum RoleId
                        using (SqlCommand getMaxRoleIdCmd = new SqlCommand("SELECT MAX(RoleId) FROM Roles", connection))
                        {
                            object maxRoleIdObj = getMaxRoleIdCmd.ExecuteScalar();
                            if (maxRoleIdObj != DBNull.Value)
                            {
                                roleId = Convert.ToInt32(maxRoleIdObj) + 1;
                            }
                            else
                            {
                                roleId = 1;
                            }
                        }

                        // Insert role
                        using (SqlCommand cmd1 = new SqlCommand("INSERT INTO Roles (RoleId, RoleName) VALUES (@RoleId, @RoleName); SELECT SCOPE_IDENTITY();", connection))
                        {
                            cmd1.Parameters.AddWithValue("@RoleName", "User");
                            cmd1.Parameters.AddWithValue("@RoleId", roleId);
                            cmd1.ExecuteNonQuery();
                            //roleId = Convert.ToInt32(cmd1.ExecuteScalar());
                        }

                        // Check if user with the email already exists
                        using (SqlCommand checkUserCmd = new SqlCommand("SELECT COUNT(*) FROM Users WHERE Email = @Email", connection))
                        {
                            checkUserCmd.Parameters.AddWithValue("@Email", email);
                            object userCountObj = checkUserCmd.ExecuteScalar();
                            int userCount = userCountObj != DBNull.Value && userCountObj != null ? Convert.ToInt32(userCountObj) : 0;
                            if (userCount > 0)
                            {
                                throw new Exception("User with this email has already been registered");
                            }
                        }

                        // Insert user
                        using (SqlCommand cmd = new SqlCommand("signup", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Action", "INSERT");
                            cmd.Parameters.AddWithValue("@Username", username);
                            cmd.Parameters.AddWithValue("@Email", email);
                            cmd.Parameters.AddWithValue("@Password", password);
                            cmd.Parameters.AddWithValue("@RoleId", roleId);
                            cmd.Parameters.AddWithValue("@MacAddress", macadress);
                            cmd.Parameters.AddWithValue("@IpAddress", ipAdress);
                            cmd.ExecuteNonQuery();
                        }
                        using (SqlCommand getUserIdCmd = new SqlCommand("SELECT UserId FROM Users WHERE Email = @Email", connection))
                        {
                            getUserIdCmd.Parameters.AddWithValue("@Email", email);
                            object userIdObj = getUserIdCmd.ExecuteScalar();
                            userId = userIdObj != DBNull.Value && userIdObj != null ? Convert.ToInt32(userIdObj) : 0;
                        }
                        // Commit transaction if all operations succeed                            
                        signUplblmsg.Text = "User created successfully";
                        signUplblmsg.ForeColor = Color.Green;
                        Response.Redirect($"{previousPageUrl}&UserId={userId}");
                    }
                    catch (Exception ex)
                    {
                        // Rollback transaction if any operation 
                        signUplblmsg.Text = ex.Message;
                        signUplblmsg.ForeColor = Color.Red;
                    }
                }

            }
            catch (Exception ex)
            {
                signUplblmsg.Text = ex.Message;
                signUplblmsg.ForeColor = Color.Red;
            }
            finally
            {
                confirmPassword = "";
                username = "";
                password = "";
                email = "";
            }

        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            string email = txtEmailSignIn.Text;
            string password = txtPasswordSignIn.Text;
            int userId = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    con.Open();
                    // Construct SQL query string
                    string query = "SELECT UserId, Username, RoleId FROM Users WHERE email = @Email AND password = @Password";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@Email", email);
                        cmd.Parameters.AddWithValue("@Password", password);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Session["UserId"] = reader["UserId"];
                                Session["Username"] = reader["Username"];
                                Session["RoleId"] = reader["RoleId"];
                            }
                            using (SqlCommand getUserIdCmd = new SqlCommand("SELECT UserId FROM Users WHERE Email = @Email", con))
                            {
                                getUserIdCmd.Parameters.AddWithValue("@Email", email);
                                object userIdObj = getUserIdCmd.ExecuteScalar();
                                userId = userIdObj != DBNull.Value && userIdObj != null ? Convert.ToInt32(userIdObj) : 0;
                            }
                            Response.Redirect($"{previousPageUrl}&UserId={userId}");
                        }
                        else
                        {
                            signinLabel.Text = "Invalid email or password";
                            signinLabel.ForeColor = Color.Red;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                signinLabel.Text = ex.Message;
                signinLabel.ForeColor = Color.Red;
            }
        }
        private (bool, int) CheckUserByMacOrIpAddress(string macAddress, string ipAddress)
        {
            bool userFound = false;
            int userId = 0;
            string connectionString = Utils.getConnection();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Check if a user with the same MAC address exists
                using (SqlCommand checkUserByMacCmd = new SqlCommand("SELECT UserId FROM UserNetworkInfo WHERE MacAddress = @MacAddress", connection))
                {
                    checkUserByMacCmd.Parameters.AddWithValue("@MacAddress", macAddress);
                    object userIdObj = checkUserByMacCmd.ExecuteScalar();
                    if (userIdObj != null && userIdObj != DBNull.Value)
                    {
                        userFound = true;
                        userId = Convert.ToInt32(userIdObj);
                    }
                }
            }

            return (userFound, userId);
        }

        // Method to sign in the user automatically
        private void SignInUserAutomatically(int id)
        {
            Response.Redirect($"{previousPageUrl}&UserId={id}");
        }
    }
}