﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="ShopDetail.aspx.cs" Inherits="JSM.User.ShopDetail" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function addToCart() {
            console.log("addToCart function called!");
            // Get color, size, product ID, and quantity
            var color = document.querySelector('input[name="colorRadioButtonList"]:checked').value;
            var size = document.querySelector('input[name="sizeRadioButtonList"]:checked').value;
            var productId = "<%= Convert.ToInt32(Request.QueryString["ProductId"]) %>"; // Assuming you have productId as a server-side variable
            var quantity = document.getElementById('<%= txtQuantity.ClientID %>').value;

            // Perform further actions here, such as submitting the form with these parameters
            // For demonstration, we'll log the details to console
            console.log("Color: " + color + ", Size: " + size + ", Product ID: " + productId + ", Quantity: " + quantity);

            // Prevent default form submission
            return false;
        }
        document.addEventListener("DOMContentLoaded", function () {
            // Get the input field
            var inputField = document.querySelector('.input-group .form-control');

            // Get the plus and minus buttons
            var plusBtn = document.querySelector('.btn-plus');
            var minusBtn = document.querySelector('.btn-minus');

            // Add click event listener to plus button
            plusBtn.addEventListener('click', function () {
                // Increment value by 1
                inputField.value = parseInt(inputField.value) + 1;
            });

            // Add click event listener to minus button
            minusBtn.addEventListener('click', function () {
                // Ensure value doesn't go below 1
                if (parseInt(inputField.value) > 1) {
                    // Decrement value by 1
                    inputField.value = parseInt(inputField.value) - 1;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page Header Start -->
    <div class="container-fluid bg-secondary mb-5">
        <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 300px">
            <h1 class="font-weight-semi-bold text-uppercase mb-3">Shop Detail</h1>
            <div class="d-inline-flex">
                <p class="m-0"><a href="Default.aspx">Home</a></p>
                <p class="m-0 px-2">-</p>
                <p class="m-0">Shop Detail</p>
            </div>
        </div>
    </div>
    <!-- Page Header End -->


    <!-- Shop Detail Start -->
    <div class="container-fluid py-5">
        <div class="row px-xl-5">
            <div class="col-lg-5 pb-5">
                <div class="md-4">
                    <asp:Label ID="CartDetails" runat="server"></asp:Label>
                </div>
                <div id="product-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner border">
                        <% 
                            // Retrieve images from the database
                            DataTable imageTable = getImages();
                            if (imageTable.Rows.Count > 0)
                            {
                                // Loop through each row in the DataTable
                                foreach (DataRow row in imageTable.Rows)
                                {
                                    // Loop through each column (image) in the current row
                                    foreach (DataColumn col in imageTable.Columns)
                                    {
                                        // Get the image URL from the current column
                                        string imageUrl = row[col].ToString();
                                        // Set the active class for the first image
                                        string activeClass = col.Ordinal == 0 ? "active" : "";
                        %>
                        <div class="carousel-item <%= activeClass %>">
                            <img class="w-100 h-100" src="<%= ResolveUrl(imageUrl) %>" alt="Image" style="object-fit: cover;">
                        </div>
                        <% 
                                    }
                                }
                            }
                            else
                            {
                                // If no images are found in the database, display a default message or image
                        %>
                        <div class="carousel-item active">
                            <img class="w-100 h-100" src="../UserTemplate/img/default-image.jpg" alt="Default Image">
                        </div>
                        <% } %>
                    </div>
                    <a class="carousel-control-prev" href="#product-carousel" data-slide="prev">
                        <i class="fa fa-2x fa-angle-left text-dark"></i>
                    </a>
                    <a class="carousel-control-next" href="#product-carousel" data-slide="next">
                        <i class="fa fa-2x fa-angle-right text-dark"></i>
                    </a>
                </div>
            </div>


            <div class="col-lg-7 pb-5">
                <%--<h3 class="font-weight-semi-bold">Colorful Stylish Shirt</h3>--%>
                <%--<div class="d-flex mb-3">
                    <div class="text-primary mr-2">
                        <small class="fas fa-star"></small>
                        <small class="fas fa-star"></small>
                        <small class="fas fa-star"></small>
                        <small class="fas fa-star-half-alt"></small>
                        <small class="far fa-star"></small>
                    </div>
                    <small class="pt-1">(50 Reviews)</small>
                </div>--%>
                <h3 class="font-weight-semi-bold mb-4">
                    <asp:Label ID="productNameLabel" runat="server" CssClass="h2"></asp:Label>
                </h3>

                <p class="mb-4">
                    <asp:Label ID="shortDescriptionLabel" runat="server" CssClass="h5"></asp:Label>
                </p>

                <h3 class="font-weight-semi-bold mb-4">
                    <asp:Label ID="priceLabel" runat="server"></asp:Label>
                </h3>


                <p class="mb-4">
                    <asp:Label ID="Manufacturer" runat="server" CssClass="h5"></asp:Label>
                </p>
                <div class="d-flex mb-3 row">
                    <p class="text-dark font-weight-medium mb-0 mr-3">Sizes:</p>
                    <asp:RadioButtonList ID="sizeRadioButtonList" runat="server" CssClass="d-flex flex-row"></asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="sizeValidator" ErrorMessage="Select One Size" ControlToValidate="sizeRadioButtonList" runat="server" />
                </div>

                <div class="d-flex mb-4">
                    <p class="text-dark font-weight-medium mb-0 mr-3">Colors:</p>
                    <asp:RadioButtonList ID="colorRadioButtonList" runat="server" CssClass="d-flex flex-row"></asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="colorValidator" ErrorMessage="Select One Color" ControlToValidate="colorRadioButtonList" runat="server" />

                </div>

                <%--<div class="col-lg-5 pb-5">
                    <div class="carousel-inner border">
                        <div class="carousel-item active">
                            <asp:Image ID="productImage" runat="server" CssClass="w-100 h-100" />
                        </div>
                    </div>
                </div>--%>

                <div class="d-flex align-items-center mb-4 pt-2">
                    <div class="input-group quantity mr-3" style="width: 130px;">
                        <div class="input-group-btn">
                            <asp:Button ID="btnMinus" runat="server" CssClass="btn btn-primary btn-minus" Text="-" OnClientClick="return false;" />
                        </div>
                        <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control bg-secondary text-center" Text="1"></asp:TextBox>
                        <div class="input-group-btn">
                            <asp:Button ID="btnPlus" runat="server" CssClass="btn btn-primary btn-plus" Text="+" OnClientClick="return false;" />
                        </div>
                    </div>
                    <asp:Button ID="btnAddtoCart" runat="server" CssClass="btn btn-primary px-3" Text="Add To Cart" OnClick="btnAddtoCart_Click" />
                    <%--<button class="btn btn-primary px-3"><i class="fa fa-shopping-cart mr-1" OnClientClick="return addToCart();"></i>Add To Cart</button>--%>
                </div>
                <div class="d-flex pt-2">
                    <p class="text-dark font-weight-medium mb-0 mr-2">Share on:</p>
                    <div class="d-inline-flex">
                        <a class="text-dark px-2" href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="text-dark px-2" href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a class="text-dark px-2" href="#">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                        <a class="text-dark px-2" href="#">
                            <i class="fab fa-pinterest"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row px-xl-5">
            <div class="col">
                <div class="nav nav-tabs justify-content-center border-secondary mb-4">
                    <a class="nav-item nav-link active" data-toggle="tab" href="#tab-pane-1">Description</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#tab-pane-2">Information</a>
                    <%--<a class="nav-item nav-link" data-toggle="tab" href="#tab-pane-3">Reviews (0)</a>--%>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <h4 class="mb-3">Product Description</h4>
                        <p class="mb-4">
                            <asp:Label ID="longDescriptionLabel" runat="server"></asp:Label>
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tab-pane-2">
                        <h4 class="mb-3">Additional Information</h4>
                        <p class="mb-4">
                            <asp:Label ID="additionalDescriptionLabel" runat="server"></asp:Label>
                        </p>
                    </div>
                    <%--                    <div class="tab-pane fade" id="tab-pane-3">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="mb-4">1 review for "Colorful Stylish Shirt"</h4>
                                <div class="media mb-4">
                                    <img src="../UserTemplate/img/user.jpg" alt="Image" class="img-fluid mr-3 mt-1" style="width: 45px;">
                                    <div class="media-body">
                                        <h6>John Doe<small> - <i>01 Jan 2045</i></small></h6>
                                        <div class="text-primary mb-2">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star-half-alt"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <p>Diam amet duo labore stet elitr ea clita ipsum, tempor labore accusam ipsum et no at. Kasd diam tempor rebum magna dolores sed sed eirmod ipsum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4 class="mb-4">Leave a review</h4>
                                <small>Your email address will not be published. Required fields are marked *</small>
                                <div class="d-flex my-3">
                                    <p class="mb-0 mr-2">Your Rating * :</p>
                                    <div class="text-primary">
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                </div>
                                <form>
                                    <div class="form-group">
                                        <label for="message">Your Review *</label>
                                        <textarea id="message" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Your Name *</label>
                                        <input type="text" class="form-control" id="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Your Email *</label>
                                        <input type="email" class="form-control" id="email">
                                    </div>
                                    <div class="form-group mb-0">
                                        <input type="submit" value="Leave Your Review" class="btn btn-primary px-3">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>
    <!-- Shop Detail End -->


    <!-- Products Start -->
    <div class="container-fluid py-5">
        <div class="text-center mb-4">
            <h2 class="section-title px-5"><span class="px-2">You May Also Like</span></h2>
        </div>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">
                    <%  
                        DataTable recommendedProducts = getRecommendedProducts();
                        foreach (DataRow row in recommendedProducts.Rows)
                        { %>
                    <div class="card product-item border-0">
                        <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                            <img class="img-fluid w-100" src="<%= row["ImageUrl"] %>" alt="">
                        </div>
                        <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                            <h6 class="text-truncate mb-3"><%= row["ProductName"] %></h6>
                            <div class="d-flex justify-content-center">
                                <h6><%= row["Price"] %> PKR</h6>
                                <%--<h6 class="text-muted ml-2"><del>$<%= row["OldPrice"] %></del></h6>--%>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-between bg-light border">
                            <a href="productDetailPage.aspx?id=<%= row["ProductId"] %>" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                            <a href="addToCart.aspx?id=<%= row["ProductId"] %>" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>

    </div>
    <!-- Products End -->
</asp:Content>
