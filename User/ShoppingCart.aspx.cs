﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.User
{
    public partial class ShoppingCart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.Url.AbsoluteUri.Contains("UserId"))
            {
                string currentUrl = $"{Request.Url.AbsoluteUri.ToString()}?";
                Response.Redirect($"signup.aspx?PreviousUrl={Server.UrlEncode(currentUrl)}");
            }
            else
            {
                if (!IsPostBack)
                {
                    DataTable cartDetails = getCartDetails();
                    Repeater1.DataSource = cartDetails;
                    Repeater1.DataBind();
                    CalculateCartSummary();
                }
            }

        }


        public DataTable getCartDetails()
        {
            DataTable cartTable = new DataTable();
            string userIdStr = Request.QueryString["UserId"];
            int userId;
            bool userIdParsed = int.TryParse(userIdStr, out userId);
            using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand("cart_crud", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.Parameters.AddWithValue("@Action", "GETALL");
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(cartTable);
                    }
                }
            }
            return cartTable;
        }
        public static void UpdateCartQuantity(int productId, int userId, int quantity)
        {
            int id = productId;
            int user = userId;
            int qty = quantity;
            // Implement logic to update the cart quantity in the database or session
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Button btnRemove = (Button)sender;
            try
            {
                int productId = Convert.ToInt32(btnRemove.CommandArgument);
                int userId = Convert.ToInt32(Request.QueryString["UserId"]);

                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand("cart_crud", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserId", userId);
                        cmd.Parameters.AddWithValue("@ProductId", productId);
                        cmd.Parameters.AddWithValue("@Action", "DELETECART");
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                DataTable cartDetails = getCartDetails();
                Repeater1.DataSource = cartDetails;
                Repeater1.DataBind();
                CalculateCartSummary();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "removeError", "alert('An error occurred while removing the item from the cart.');", true);
            }
        }

        protected void btnPlus_Click(object sender, EventArgs e)
        {
            Button btnPlus = (Button)sender;
            try
            {
                string productIdStr = btnPlus.CommandArgument;
                int productId = Convert.ToInt32(btnPlus.CommandArgument);
                // update the quantity in the cart by 1 
                int userId = Convert.ToInt32(Request.QueryString["UserId"]);
                RepeaterItem item = (RepeaterItem)btnPlus.NamingContainer;
                TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand("cart_op", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserId", userId);
                        cmd.Parameters.AddWithValue("@ProductId", productId);
                        cmd.Parameters.AddWithValue("@Action", "ADDCART");
                        cmd.Parameters.AddWithValue("@UpdatedQuantity", SqlDbType.Int).Direction = ParameterDirection.Output;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        int updatedQuantity = Convert.ToInt32(cmd.Parameters["@UpdatedQuantity"].Value);
                        txtQuantity.Text = updatedQuantity.ToString();
                    }
                    DataTable cartDetails = getCartDetails();
                    Repeater1.DataSource = cartDetails;
                    Repeater1.DataBind();
                    CalculateCartSummary();
                }
            }
            catch (FormatException ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "formatError", "alert('Invalid product ID format.');", true);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Button btnPlus = (Button)sender;
            try
            {
                string productIdStr = btnPlus.CommandArgument;
                int productId = Convert.ToInt32(btnPlus.CommandArgument);
                // update the quantity in the cart by 1 
                int userId = Convert.ToInt32(Request.QueryString["UserId"]);
                RepeaterItem item = (RepeaterItem)btnPlus.NamingContainer;
                TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand("cart_op", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserId", userId);
                        cmd.Parameters.AddWithValue("@ProductId", productId);
                        cmd.Parameters.AddWithValue("@Action", "SUBCART");
                        cmd.Parameters.AddWithValue("@UpdatedQuantity", SqlDbType.Int).Direction = ParameterDirection.Output;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        int updatedQuantity = Convert.ToInt32(cmd.Parameters["@UpdatedQuantity"].Value);
                        txtQuantity.Text = updatedQuantity.ToString();
                    }
                    DataTable cartDetails = getCartDetails();
                    Repeater1.DataSource = cartDetails;
                    Repeater1.DataBind();
                    CalculateCartSummary();
                }
            }
            catch (FormatException ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "formatError", "alert('Invalid product ID format.');", true);
            }
        }
        private void CalculateCartSummary()
        {
            DataTable cartDetails = getCartDetails(); // Get cart details again to ensure consistency
            decimal subtotal = 0;
            foreach (DataRow row in cartDetails.Rows)
            {
                int quantity = Convert.ToInt32(row["Quantity"]);
                decimal price = Convert.ToDecimal(row["Price"]);
                subtotal += quantity * price;
            }
            // Set subtotal
            lblSubtotal.Text = subtotal.ToString() + " RS"; // Assuming lblSubtotal is the label to display the subtotal
            decimal shipping = 0; // Change this value if shipping is calculated dynamically
            if (subtotal >= 3000)
            {
                shipping = 0;
            }
            else if (subtotal > 0)
            {
                shipping = 200;
            }
            // Set shipping (assuming it's a fixed value)
            lblShipping.Text = shipping.ToString() + " RS";

            //// Calculate total
            decimal total = subtotal + shipping;
            lblTotal.Text = total.ToString() + " RS";
        }

        protected void checkoutBtn_Click(object sender, EventArgs e)
        {
            string UserId = Request.QueryString["UserId"];
            string url = $"Checkout.aspx?&UserId={UserId}";
            if (!string.IsNullOrEmpty(UserId))
            {
                Response.Redirect(url);
            }
        }
    }
}