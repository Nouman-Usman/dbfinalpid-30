﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.User
{
    public partial class Orders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        void LoadData()
        {
            int UserId = Convert.ToInt32(Request.QueryString["UserId"]);
            pdTable.DataSource = null;
            pdTable.DataBind();
            SqlConnection con = new SqlConnection(Utils.getConnection());
            SqlCommand cmd = new SqlCommand("shipping_Crud", con);
            cmd.Parameters.AddWithValue("@Action", "GETALL");
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable da = new DataTable();
            sda.Fill(da);
            pdTable.DataSource = da;
            pdTable.DataBind();
        }
    }
}