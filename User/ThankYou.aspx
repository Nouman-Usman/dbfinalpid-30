﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="ThankYou.aspx.cs" Inherits="JSM.User.ThankYou" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 align-content-center">
                <h1 class="text-center">Thank You!</h1>
                <p class="text-center">Your Order has been Placed successfully.</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <asp:Button ID="goBackButton" runat="server" Text="Back To Home" CssClass="btn btn-primary" OnClick="goBackButton_Click" />
        </div>
    </div>
</asp:Content>
