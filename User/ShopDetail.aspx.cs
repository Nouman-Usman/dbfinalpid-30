﻿using JSM.Admin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static JSM.Admin.Product;

namespace JSM.User
{
    public partial class ShopDetail : System.Web.UI.Page
    {

        int category = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (Request.Url.AbsoluteUri.Contains("UserId"))
                //{
                //    string previousUrl= Request.Url.AbsoluteUri;
                //    string productIdStr = Request.QueryString["ProductId"];
                //    string quantityStr = Request.QueryString["Quantity"];
                //    string userIdStr = Request.QueryString["UserId"];
                //    string color = Request.QueryString["Color"].ToString();
                //    string size = Request.QueryString["Size"].ToString();
                //    int productId, quantity, userId;
                //    bool productIdParsed = int.TryParse(productIdStr, out productId);
                //    bool quantityParsed = int.TryParse(quantityStr, out quantity);
                //    bool userIdParsed = int.TryParse(userIdStr, out userId);
                //    if (!IsPostBack)
                //    {
                //        using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                //        {
                //            con.Open();
                //            using (SqlCommand cmd = new SqlCommand("cart_crud", con))
                //            {
                //                cmd.CommandType = CommandType.StoredProcedure;
                //                cmd.Parameters.AddWithValue("@UserId", userId);
                //                cmd.Parameters.AddWithValue("@ProductId", productId);
                //                cmd.Parameters.AddWithValue("@Quantity", quantity);
                //                cmd.Parameters.AddWithValue("@Color", color);
                //                cmd.Parameters.AddWithValue("@Size", size);
                //                cmd.Parameters.AddWithValue("@Action", "INSERT");
                //                cmd.ExecuteNonQuery();
                //            }
                //        }
                //    }
                //    CartDetails.Text = "Product added to Cart successfully!";
                //    CartDetails.CssClass = "alert alert-success";
                //}
                
                {
                    LoadProductDetails();
                }
            }

        }
        public DataTable LoadDetails()
        {
            int productId = Convert.ToInt32(Request.QueryString["ProductId"]);
            DataTable productTable = new DataTable();
            using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
            {
                //command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                string query = $"SELECT * FROM Product WHERE ProductId = {productId}";
                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    cmd.Parameters.AddWithValue("@ProductId", productId);
                    using (SqlDataAdapter adapter1 = new SqlDataAdapter(cmd))
                    {
                        adapter1.Fill(productTable);
                    }
                }


            }
            return productTable;

        }
        protected void LoadProductDetails()
        {
            DataTable productDetails = LoadDetails();
            if (productDetails.Rows.Count > 0)
            {
                DataRow productRow = productDetails.Rows[0];
                // Retrieve product details from the DataTable
                string productName = productRow["ProductName"].ToString();
                string shortDescription = productRow["ShortDescription"].ToString();
                string longDescription = productRow["LongDescription"].ToString();
                string additionalDescription = productRow["AdditionalDescription"].ToString();
                decimal price = Convert.ToDecimal(productRow["Price"]);
                string sizes = productRow["Size"].ToString();
                string colors = productRow["Color"].ToString();
                string manufacturer = productRow["CompanyName"].ToString();
                category = Convert.ToInt32(productRow["CategoryId"]);
                if (manufacturer == "")
                {
                    manufacturer = "Self";
                }
                // Display product details in the HTML elements
                productNameLabel.Text = productName;
                shortDescriptionLabel.Text = shortDescription;
                longDescriptionLabel.Text = longDescription;
                additionalDescriptionLabel.Text = additionalDescription;
                //priceLabel.Text = string.Format("{0:C}", price);
                priceLabel.Text = price + " PKR";
                Manufacturer.Text = "Manufacturer: " + manufacturer;
                // Populate sizes
                PopulateSizes(sizes);

                // Populate colors
                PopulateColors(colors);
            }
        }
        protected void PopulateSizes(string sizes)
        {
            // Split sizes into an array
            string[] sizeArray = sizes.Split(',');
            foreach (string size in sizeArray)
            {
                // Create radio button for each size
                ListItem item = new ListItem(size.Trim(), size.Trim());
                sizeRadioButtonList.Items.Add(item);
            }
        }

        protected void PopulateColors(string colors)
        {
            // Split colors into an array
            string[] colorArray = colors.Split(',');
            foreach (string color in colorArray)
            {
                // Create radio button for each color
                ListItem item = new ListItem(color.Trim(), color.Trim());
                colorRadioButtonList.Items.Add(item);
            }
        }
        public DataTable getImages()
        {
            DataTable imageTable = new DataTable();
            int productId = Convert.ToInt32(Request.QueryString["ProductId"]);
            using (SqlConnection con = new SqlConnection(Utils.getConnection()))
            {
                con.Open();
                string query = $"SELECT image1, image2, image3, image4 FROM productAddImages WHERE ProductId = {productId}";
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@ProductId", productId);
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(imageTable);
                    }
                }
            }
            return imageTable;
        }

        public DataTable getRecommendedProducts()
        {
            DataTable products = new DataTable();
            using (SqlConnection con = new SqlConnection(Utils.getConnection()))
            {
                con.Open();
                string query = "SELECT TOP 6 p.ProductID, p.ProductName, p.ShortDescription, p.LongDescription, p.AdditionalDescription, p.Price, p.Quantity, p.Size, p.Color, p.CompanyName, p.CategoryId, p.Sold, p.IsCustiomized, p.IsActive, p.CreatedDate, pi.ImageUrl \r\nFROM Product p \r\nLEFT JOIN ProductImages pi ON p.ProductID = pi.ProductID \r\nWHERE p.IsActive = 1 AND p.CategoryId = @CategoryId \r\nORDER BY p.ProductName;";
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@CategoryId", category);
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(products);
                    }
                }
            }
            return products;
        }

        protected void btnAddtoCart_Click(object sender, EventArgs e)
        {
            // If already signed in, proceed to add the product to the cart
            string color = Request.Form[colorRadioButtonList.UniqueID];
            string size = Request.Form[sizeRadioButtonList.UniqueID];
            int productId = Convert.ToInt32(Request.QueryString["ProductId"]);
            int quantity = Convert.ToInt32(txtQuantity.Text);
            string previousUrl = Request.Url.AbsoluteUri;
            string productIdStr = Request.QueryString["ProductId"];
            string quantityStr = Request.QueryString["Quantity"];
            string userIdStr = Request.QueryString["UserId"];
            int userId;
            bool userIdParsed = int.TryParse(userIdStr, out userId);
            try
            {
                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("cart_crud", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserId", userId);
                        cmd.Parameters.AddWithValue("@ProductId", productId);
                        cmd.Parameters.AddWithValue("@Quantity", quantity);
                        cmd.Parameters.AddWithValue("@Color", color);
                        cmd.Parameters.AddWithValue("@Size", size);
                        cmd.Parameters.AddWithValue("@Action", "INSERT");
                        cmd.ExecuteNonQuery();
                    }
                }
                CartDetails.Text = "Product added to Cart successfully!";
                CartDetails.CssClass = "alert alert-success";
                //string currentUrl = $"{Request.Url.AbsoluteUri}&Color={color}&Size={size}&Quantity={quantity}";
                //Response.Redirect($"ShoppingCart.aspx?&UserId={userId}");
            }
            catch (Exception  ex){
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Add Error", "alert('An error occurred while Adding this item to the cart.');", true);

            }

        }
    }
}