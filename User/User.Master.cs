﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.User
{
    public partial class User : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Url.AbsoluteUri.ToString().Contains("Default.aspx"))
            {
                Control slideControl = (Control)Page.LoadControl("SliderUserControl.ascx");
                SliderUC.Controls.Add(slideControl);
            }
            cartItemCount.Text = getCartItemCount().ToString();
            heartLink.Text = getOrderCount().ToString();
        }

        public int getCartItemCount()
        {
            int cartItemCount = 0;
            if (Request.Url.AbsoluteUri.Contains("UserId"))
            {
                string userIdStr = Request.QueryString["UserId"];
                if (userIdStr != null)
                {

                    int userId = Convert.ToInt32(userIdStr);
                    using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
                    {
                        // Get the username from the user ID
                        SqlCommand sqlCommand = new SqlCommand("SELECT Username FROM Users WHERE UserId = @UserId", connection);
                        sqlCommand.Parameters.AddWithValue("@UserId", userId);
                        connection.Open();
                        string username = sqlCommand.ExecuteScalar().ToString();
                        loginLink.Text = username;
                        //loginLink.NavigateUrl = $"User/Profile.aspx?UserId={userId}";
                        using (SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM Cart WHERE UserId = @UserId", connection))
                        {
                            command.Parameters.AddWithValue("@UserId", userId); // Pass the user ID here
                            //connection.Open();
                            object result = command.ExecuteScalar();
                            if (result != null)
                            {
                                cartItemCount = Convert.ToInt32(result);
                            }
                        }
                    }
                }
            }
            else
            {
                string currentUrl = $"{Request.Url.AbsoluteUri.ToString()}?";
                Response.Redirect($"signup.aspx?PreviousUrl={Server.UrlEncode(currentUrl)}");
            }
            return cartItemCount;
        }
        int getOrderCount()
        {
            int totalOrders = 0;
            if(Request.Url.AbsoluteUri.Contains("UserId"))
            {
                string userIdStr = Request.QueryString["UserId"];
                if (userIdStr != null)
                {

                    int userId = Convert.ToInt32(userIdStr);
                    using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
                    {
                        // Get the username from the user ID
                        SqlCommand sqlCommand = new SqlCommand("SELECT Username FROM Users WHERE UserId = @UserId", connection);
                        sqlCommand.Parameters.AddWithValue("@UserId", userId);
                        connection.Open();
                        string username = sqlCommand.ExecuteScalar().ToString();
                        loginLink.Text = username;
                        //loginLink.NavigateUrl = $"User/Profile.aspx?UserId={userId}";
                        using (SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM Orders WHERE UserId = @UserId", connection))
                        {
                            command.Parameters.AddWithValue("@UserId", userId);
                            //connection.Open();
                            object result = command.ExecuteScalar();
                            if (result != null)
                            {
                                totalOrders = Convert.ToInt32(result);
                            }
                        }
                    }
                }

            }
            return totalOrders;
        }
        public DataTable GetAllCategories()
        {
            DataTable categoriesTable = new DataTable();
            using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
            {
                using (SqlCommand command = new SqlCommand("Category_Crud", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Action", "GETALL");

                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(categoriesTable);
                }
            }
            return categoriesTable;
        }
    }
}