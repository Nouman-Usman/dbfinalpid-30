﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="Shop.aspx.cs" Inherits="JSM.User.Shop" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function () {
            $('#<%= ColorCheckBoxList.ClientID %> input[type="checkbox"]').change(function () {
                if ($(this).prop('checked')) {
                    $('#<%= ColorCheckBoxList.ClientID %> input[type="checkbox"]').not(this).prop('checked', false);
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#<%= PriceCheckBoxList.ClientID %> input[type="checkbox"]').change(function () {
                if ($(this).prop('checked')) {
                    $('#<%= PriceCheckBoxList.ClientID %> input[type="checkbox"]').not(this).prop('checked', false);
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#<%= SizeCheckBoxList.ClientID %> input[type="checkbox"]').change(function () {
                if ($(this).prop('checked')) {
                    $('#<%= SizeCheckBoxList.ClientID %> input[type="checkbox"]').not(this).prop('checked', false);
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page Header Start -->
    <div class="container-fluid bg-secondary mb-5">
        <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 300px">
            <h1 class="font-weight-semi-bold text-uppercase mb-3">Welcome To Our Shop</h1>
            <div class="d-inline-flex">
                <p class="m-0"><a href="Default.aspx">Home</a></p>
                <p class="m-0 px-2">-</p>
                <p class="m-0">Shop</p>
            </div>
        </div>
    </div>
    <!-- Page Header End -->


    <!-- Shop Start -->
    <div class="container-fluid pt-5">
        <div class="row px-xl-5">
            <!-- Shop Sidebar Start -->
            <div class="col-lg-3 col-md-12">
                <!-- Price Start -->
                <div class="border-bottom mb-4 pb-4">
                    <h5 class="font-weight-semi-bold mb-4">Filter by price</h5>
                    <asp:CheckBoxList ID="PriceCheckBoxList" runat="server" CssClass="custom-control ">
                        <asp:ListItem Text="0 - 100" Value="1" />
                        <asp:ListItem Text="100 - 500" Value="2" />
                        <asp:ListItem Text="600 - 1000" Value="3" />
                        <asp:ListItem Text="1100 - 1500" Value="4" />
                        <asp:ListItem Text="1600 - 2000" Value="5" />
                        <asp:ListItem Text="2100 - 2500" Value="5" />
                    </asp:CheckBoxList>
                </div>
                <!-- Price End -->

                <!-- Color Start -->
                <div class="border-bottom mb-4 pb-4">
                    <h5 class="font-weight-semi-bold mb-4">Filter by color</h5>
                    <asp:CheckBoxList ID="ColorCheckBoxList" runat="server" CssClass="custom-control">
                        <asp:ListItem Text="Black" Value="Black" />
                        <asp:ListItem Text="White" Value="White" />
                        <asp:ListItem Text="Red" Value="Red" />
                        <asp:ListItem Text="Blue" Value="Blue" />
                        <asp:ListItem Text="Green" Value="Green" />
                    </asp:CheckBoxList>
                </div>
                <!-- Color End -->

                <!-- Size Start -->
                <div class="mb-5">
                    <h5 class="font-weight-semi-bold mb-4">Filter by size</h5>
                    <asp:CheckBoxList ID="SizeCheckBoxList" runat="server" CssClass="custom-control">
                        <asp:ListItem Text="S" Value="S" />
                        <asp:ListItem Text="M" Value="M" />
                        <asp:ListItem Text="L" Value="L" />
                        <asp:ListItem Text="XL" Value="XL" />
                    </asp:CheckBoxList>
                </div>
                <!-- Size End -->
                <form>
                    <div class="form-action pb-5">
                        <div class="text-left">
                            <asp:Button ID="btnAddOrUpdate" runat="server" CssClass="btn btn-info" Text="Apply" OnClick="btnAddOrUpdate_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-danger" Text="Reset" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </form>
            </div>
            <!-- Shop Sidebar End -->


            <!-- Shop Product Start -->
            <div class="col-lg-9 col-md-12">
                <div class="row pb-3">
                    <asp:Repeater ID="ProductRepeater" runat="server">
                        <ItemTemplate>
                            <div class="col-lg-4 col-md-6 col-sm-12 pb-1">
                                <div class="card product-item border-0 mb-4">
                                    <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                                        <img class="img-fluid w-100" src='<%# Eval("ImageUrl") %>' alt="" style="width: 100%; height: 200px;" />
                                    </div>
                                    <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                                        <h6 class="text-truncate mb-3"><%# Eval("ProductName") %></h6>
                                        <div class="d-flex justify-content-center">
                                            <h6><%# Eval("Price") %> PKR</h6>
                                        </div>
                                    </div>
                                    <div class="card-footer d-flex justify-content-between bg-light border">
                                        <a href='<%# $"shopDetail.aspx?&ProductId=" + Eval("ProductId") + "&UserId=" + Request.QueryString["UserId"] %>' class="btn btn-sm text-dark p-0">
                                            <i class="fas fa-eye text-primary mr-1"></i>View Detail
                                        </a>
                                        <a href='<%# "ShopDetail.aspx?ProductId=" + Eval("ProductId") + "&UserId=" + Request.QueryString["UserId"] %>' class="btn btn-sm text-dark p-0">
                                            <i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>

            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
</asp:Content>
